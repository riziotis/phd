#!/usr/bin/env python3

import subprocess
import os

def main():
    """Submits multiple jobs to LSF"""

    input_path = '/nfs/research1/thornton/riziotis/research/phd/datasets/csa3d/entries/entries/'
    log_path = '/nfs/research1/thornton/riziotis/research/phd/analyses/csa3d_cofactor_sites/logs/'
    for dir in (input_path, log_path):
        if not os.path.exists(dir):
            os.makedirs(dir)

    # Big memory entries
    with open('/nfs/research1/thornton/riziotis/research/csa3d/tests/bigmem.list', 'r') as f:
        bigmem = set([int(entry.strip()) for entry in f.readlines()])

    for i, infile in enumerate(os.listdir(input_path)):

        mcsa_id = int(infile.split('.')[0].split('_')[-1])
        logfile = os.path.join(log_path, '{}.log'.format(infile.split('.')[0]))
        infile = os.path.join(input_path, infile)


        memory = 15000
        if mcsa_id in bigmem:
            memory = 60000

        cmd = 'bsub -M {} -o {} python classify_per_cofactor.py {}'.format(memory, logfile, infile)
        subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

