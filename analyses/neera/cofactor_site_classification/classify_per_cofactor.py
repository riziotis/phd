#!/usr/bin/env python3

import sys
import os
import pickle
from Bio.PDB import PDBIO

with open('./pdb_cofactors.list', 'r') as f:
    COFACTORS = [c.strip() for c in f.readlines()]
    
def main(csa3d_entry):
    with open(csa3d_entry, 'rb') as f:
        entry = pickle.load(f)

    io = PDBIO()
    for pdbsite in entry.pdbsites:
        for cofactor in COFACTORS:
            outdir = f'./per_cofactor/{cofactor}'
            for het in pdbsite.nearby_hets:
                if het.resname == cofactor:
                    os.makedirs(outdir, exist_ok=True)

                    #Transform and write site
                    rot, tran, _, _ = pdbsite.reference_site.fit(pdbsite, transform=True)
                    pdbsite.write_pdb(outdir=outdir, write_hets=True, func_atoms_only=False)

                    #Transform and write parent structure
                    pdbsite.parent_structure.transform(rot, tran)
                    io.set_structure(pdbsite.parent_structure)
                    io.save('{}/mcsa_{}.{}.full.pdb'.format(outdir, str(pdbsite.mcsa_id).zfill(4), pdbsite.pdb_id))
    
if __name__ == '__main__':
    main(sys.argv[1])

