#!/usr/bin/env python3

import subprocess
import os
from natsort import natsorted
from datetime import datetime

MEMORY = 16000
def main():
    """Submits multiple jobs to LSF"""

    rmsd_cutoff = 1.5
    distance_cutoff = 1.0
    max_total_cutoff = 2.0
    ttype = 'functional'
    
    now = datetime.now().strftime('%d-%m-%y')

    templates_path = f'/nfs/research/thornton/riziotis/research/phd/analyses/template_runs/input/templates/three_residue/{ttype}_atoms/grouped'
    targets_path = '/nfs/research/thornton/riziotis/research/phd/analyses/template_runs/input/targets/pdb'
    output_path = f'/nfs/research/thornton/riziotis/research/phd/results/template_runs/{ttype}_atoms/{now}/jessout/'
    log_path = f'/nfs/research/thornton/riziotis/research/phd/results/template_runs/{ttype}_atoms/{now}/logs/'

    for dir in (output_path, log_path):
        if not os.path.exists(dir):
            os.makedirs(dir)

    groupname = f'jess_pdb_{ttype}_optimized'
    gcmd = f'bgadd -L 350 /{groupname}'
    subprocess.run(gcmd, shell=True)
    for i,target in enumerate(natsorted(os.listdir(targets_path))):
        target_name  = target.replace('.list', '')
        target = os.path.join(targets_path, target)
        for template in natsorted(os.listdir(templates_path)):
            template_name = template.split('.')[-1]
            template = os.path.join(templates_path, template)

            name = f'{template_name}.{target_name}.{rmsd_cutoff}_{distance_cutoff}.jessout.filtered.pdb'
            outfile = os.path.join(output_path, name)
            logfile = os.path.join(log_path, f'{name.replace(".pdb", "")}.log')
        
            cmd = f'bsub -g /{groupname} -q standard -M {MEMORY} -o {logfile} "jess {template} {target} {rmsd_cutoff} {distance_cutoff} {max_total_cutoff} iqe | filterjess > {outfile}"'
            subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

