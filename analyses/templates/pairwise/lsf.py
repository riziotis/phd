#!/usr/bin/env python3

import subprocess
import os
from natsort import natsorted
from datetime import datetime

MEMORY = 8000
def main():
    """Submits multiple jobs to LSF"""

    rmsd_cutoff = 3.5
    distance_cutoff = 3.0
    max_total_cutoff = 5.0
    for ttype in ('functional', 'backbone'):
    
        now = datetime.now().strftime('%d-%m-%y')

        templates_path = f'/nfs/research/thornton/riziotis/research/phd/analyses/template_pairwise/input/templates/three_residue_permutations/{ttype}_atoms/grouped'
        targets_path = templates_path
        output_path = f'/nfs/research/thornton/riziotis/research/phd/results/template_pairwise/three_residue_permutations/{ttype}_atoms/{now}/jessout/'
        log_path = f'/nfs/research/thornton/riziotis/research/phd/results/template_pairwise/three_residue_permutations/{ttype}_atoms/{now}/logs/'

        for dir in (output_path, log_path):
            if not os.path.exists(dir):
                os.makedirs(dir)

        groupname = f'jess_pairwise_{ttype}'
        gcmd = f'bgadd -L 2000 /{groupname}'
        subprocess.run(gcmd, shell=True)
        seen = set()
        for i,target in enumerate(natsorted(os.listdir(targets_path))):
            target_name  = target.split('.')[-1]
            target = os.path.join(targets_path, target)
            for template in natsorted(os.listdir(templates_path)):
                template_name = template.split('.')[-1]
                if template_name == target_name or (target_name, template_name) in seen:
                    continue
                seen.add((template_name, target_name))
                template = os.path.join(templates_path, template)

                name = f'pairwise.{template_name}.{target_name}.{rmsd_cutoff}_{distance_cutoff}.jessout.filtered.pdb'
                outfile = os.path.join(output_path, name)
                logfile = os.path.join(log_path, f'{name.replace(".pdb", "")}.log')
            
                cmd = f'bsub -g /{groupname} -q standard -M {MEMORY} -o {logfile} "jess {template} {target} {rmsd_cutoff} {distance_cutoff} {max_total_cutoff} iqe | filterjess > {outfile}"'
                subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

