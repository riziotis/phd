#!/usr/bin/env python3

import sys
import os
import pickle
import random
import pandas as pd
import numpy as np
from scipy.spatial.distance import squareform

def main(entry_infile, comparisons_infile, outdir, matrix_infile=''):
    # Load entry
    print('Loading entry...')
    with open(entry_infile, 'rb') as entry:
        entry = pickle.load(entry)
    if len(entry.pdbsites) <= 1:
        print(f'Entry {entry.mcsa_id}: Not enough homologues, no template will be generated') 
        return
    if len(entry.pdbsites[0]) < 3:
        print(f'Entry {entry.mcsa_id}: Less than 3 residues, no template will be generated') 
        return

    # Load pairwise comparisons dataset
    print('Loading pairwise superpositions file...')
    with open(comparisons_infile, 'r') as comparisons:
        comparisons = pd.read_csv(comparisons, low_memory=False)
    if comparisons.empty:
        print(f'Entry {entry.mcsa_id}: No pairwise comparison data, reference site will be the template') 

    # Define output paths
    matrix_outfile = f'{outdir}/csa3d_{str(entry.mcsa_id).zfill(4)}.matrix_conserved.csv'
    dendro_outfile = f'{outdir}/csa3d_{str(entry.mcsa_id).zfill(4)}.dendrogram.png'

    # Get rmsd matrix for conserved sites
    print('Building RSMD matrix...')
    if os.path.isfile(matrix_infile):
        matrix = pd.read_csv(matrix_infile, index_col=0)
    else:
        matrix = get_rmsd_matrix(entry, comparisons, matrix_outfile)

    # Do clustering
    print('Clustering...')
    clusters = entry.clustering_bayesian(matrix, k_max=5, plot_outfile=dendro_outfile) 
    
    # Generate template for each cluster
    print('Building templates...')
    templates = []
    for i, cluster in clusters.items():
        print(f'    Generating template {i+1}')
        templates.append(entry.create_template(comparisons, ca=True, outdir=outdir, subset=cluster, cluster_no=i+1, no_write=True))

    print('Cleaning-up redundant templates...')
    templates = remove_redundant_templates(templates, rmsd_cutoff=1.5)

    print('Writing template coordinates...')
    for i, template in enumerate(templates):
        if None in [r.structure for r in template]:
            print('Missing atoms, cannot write template')
            continue
        # Break template in subsets of 3 residues
        max_distance = 999
        groups = entry.break_template(template, n_residues=2, permutations=True, max_distance=max_distance)
        n = len(groups)
        for j, group in enumerate(groups):
            entry.write_template(template, comparisons=comparisons, ca=True, 
                                 subset=template.subset, residues=group, 
                                 cluster_no = f'{i+1}_{j+1}_{n}',
                                 outdir=outdir)
    # All done
    print('Done!')
    return

def get_rmsd_matrix(entry, comparisons, outfile):
    """Constructs RMSD matrix of sites that are conserved or have conservative mutations"""
    ids = [p.id for p in entry.get_pdbsites() if p.is_sane and (p.is_conserved or p.is_conservative_mutation)]
    if len(ids)>100:
        random.seed(10)
        ids = random.sample(ids, 100)
    rmsds = []
    for p,q in entry.all_vs_all(sane_only=True):
        if p.id in ids and q.id in ids:
            try:
                hit = comparisons[comparisons.isin([p.id]).any(axis=1) & comparisons.isin([q.id]).any(axis=1)]
                if not hit['wrms_all'].isnull().values.any():
                    rmsd = hit['wrms_all'].values[-1]
                else:
                    rmsd = hit['rms_all'].values[-1]
            except Exception as e:
                rmsd = 1
            rmsds.append(rmsd)
    rmsds = np.array(rmsds, dtype='float32')
    matrix = squareform(rmsds)
    matrix = pd.DataFrame(matrix, columns=ids, index=ids)
    matrix.to_csv(outfile, index=True)
    return matrix

def remove_redundant_templates(templates, rmsd_cutoff=2):
    """Cleans up a list of templates from redundant templates, based on an RMSD cutoff"""
    seen = set()
    reject = set()
    for i,p in enumerate(templates):
        for j,q in enumerate(templates):
            if p.id == q.id or (q.id, p.id) in seen:
                continue
            seen.add((p.id, q.id))
            rot, tran, rms, rms_all = p.fit(q)
            if rms_all < rmsd_cutoff:
                # Keep the one representing the largest cluster
                if len(p.subset) < len(q.subset):
                    reject.add(p.id)
                else:
                    reject.add(q.id)
    nr = []
    for i,t in enumerate(templates):
        if t.id not in reject and t not in nr:
            nr.append(t)
    return nr

if __name__ == '__main__':
    if len(sys.argv) == 4:
        main(sys.argv[1], sys.argv[2], sys.argv[3]) 
    if len(sys.argv) == 5:
        main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]) 


