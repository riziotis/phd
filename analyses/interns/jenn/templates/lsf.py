#!/usr/bin/env python3

import subprocess
import os
import csv
from glob import glob
from natsort import natsorted
from datetime import datetime

def main():
    """Submits multiple jobs to LSF"""

    now = datetime.now().strftime('%d-%m-%y')
    entries_path = '/nfs/research/thornton/riziotis/research/phd/data/csa3d/entries/13-05-22/entries/'
    comparisons_path = '/nfs/research/thornton/riziotis/research/phd/data/csa3d/variation/data/per_entry/'
    # Only if we have pre-calculated matrices
    matrices_path = '/nfs/research/thornton/riziotis/research/phd/results/csa3d/template_library/rmsd_matrices/'
    output_path = f'./templates/{now}/results/'
    log_path = f'./templates/{now}/logs/'
    for dir in (output_path, log_path):
        os.makedirs(dir, exist_ok=True)

    # Subset of entries
    subset = []
    with open('../same_ec_diff_cath.csv', 'r') as f:
        for l in csv.reader(f):
            subset.extend((int(l[0]),int(l[1])))
    subset = set(subset)

    # Big memory entries
    with open('/nfs/research/thornton/riziotis/research/csa3d/tests/bigmem.list', 'r') as f:
        bigmem = set([int(entry.strip()) for entry in f.readlines()])

    subprocess.run('bgadd -L 2000 /jenn', shell=True)
    for i in natsorted(os.listdir(entries_path)):
        i = i.split('.')[0]
        mcsa_id  = int(i.split('_')[1])
        if mcsa_id not in subset:
            continue
        memory = 30000
        if mcsa_id in bigmem:
            memory = 80000

        entry = os.path.join(entries_path, f'{i}.ent')
        comparisons = os.path.join(comparisons_path, f'{i}.csv')

        #Try to get pre-calculated matrix
        try:
            matrix = glob(f'{matrices_path}/csa3d_{str(mcsa_id).zfill(4)}.*')[0]
        except IndexError:
            continue
        logfile = os.path.join(log_path, f'{i}.log')
        outdir = os.path.join(output_path, f'{i}')
        os.makedirs(outdir, exist_ok=True)

        cmd = 'bsub -g /jenn -q standard -M {} -R "rusage[mem={}]" -o {} python generate_templates.py {} {} {} {}'.format(memory, memory, logfile, entry, comparisons, outdir, matrix)
        subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

