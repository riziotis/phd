#!/usr/bin/env python3

import subprocess
import os
from natsort import natsorted
from datetime import datetime

MEMORY = 8000
def main():
    """Submits multiple jobs to LSF"""

    now = datetime.now().strftime('%d-%m-%y')

    #kind = 'sidechain'
    for kind in ('backbone', 'intermediate', 'sidechain'):
        template = f'/nfs/research/thornton/riziotis/research/phd/analyses/agb/templates/{kind}_templates.list'
        targets_path = '/nfs/research/thornton/riziotis/research/phd/analyses/agb/targets/all'
        output_path = f'/nfs/research/thornton/riziotis/research/phd/results/agb/{kind}/{now}/jessout/'
        log_path = f'/nfs/research/thornton/riziotis/research/phd/results/agb/{kind}/{now}/logs/'

        for dir in (output_path, log_path):
            if not os.path.exists(dir):
                os.makedirs(dir)

        rmsd_cutoff = 2.0
        distance_cutoff = 2.0
        maxdist_cutoff = 2.0
        template_name = os.path.basename(template).strip('.list')
        for target in natsorted(os.listdir(targets_path)):
            target_name  = target.split('.')[0]
            target = os.path.join(targets_path, target)
            name = f'{template_name}.{target_name}.{rmsd_cutoff}_{distance_cutoff}_{maxdist_cutoff}.jessout.filtered.pdb'
            outfile = os.path.join(output_path, name)
            logfile = os.path.join(log_path, f'{name.strip("pdb")}log')
            
            cmd = f'bsub -M {MEMORY} -o {logfile} "jess {template} {target} {rmsd_cutoff} {distance_cutoff} {maxdist_cutoff} iqe | filterjess > {outfile}"'
            subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

