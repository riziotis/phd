#!/usr/bin/env python3

import sys
import csv
from glob import glob
from natsort import natsorted
from ast import literal_eval
sys.path.append('../templates')
from ..templates.common import pdb_get_residues
    

def main():

    SCORES = get_similarity_scores()
    
    entries = natsorted(glob('/Users/riziotis/ebi/phd/results/stable/cat_sites/*'))
    for i, entry in enumerate(natsorted(entries)):
        mcsa_id = entry.split('_')[-1]
        sites = natsorted(glob('{}/*.pdb'.format(entry)))

        seen = set()
        for i in sites:
            i_info = get_het_info(i)
            for j in sites:
                j_info = get_het_info(j)

                if (j,i) in seen:
                    continue
                seen.add((i,j))

                ##For the time being, ignore crosschain sites
                #if not i_info or not info or i_info[1] or (i_info[0], info[0]) not in SCORES:
                #    continue
                if not i_info or not j_info:
                    continue
                if (i_info[0], j_info[0]) in SCORES:
                    scores = SCORES[(i_info[0],j_info[0])]
                elif (j_info[0], i_info[0]) in SCORES:
                    scores = SCORES[(j_info[0],i_info[0])]
                else:
                    continue

                #seq_sim = scores['seq_sim']
                #whole_chain_rmsd = scores['whole_chain_rmsd']
                cat_site_rmsd = scores['cat_site_rmsd']
                seq_sim = None
                whole_chain_rmsd = None

                print(result_string(mcsa_id, i_info, j_info, seq_sim, cat_site_rmsd, whole_chain_rmsd))


def result_string(mcsa_id, i_info, info, seq_sim, cat_site_rmsd, whole_chain_rmsd):
    result = '{},{},{},{},{},{}'.format(
                mcsa_id, ','.join(str(i) for i in i_info), 
                ','.join(str(i) for i in info),
                seq_sim, cat_site_rmsd, whole_chain_rmsd)
    return result


def get_het_info(site):
    """Gets HET component info and returns a csv string"""
    sitename = site.split('/')[-1].split('.')[1].upper()

    #Add chains to refernce site name
    if 'reference' in site:
        chains = set()
        for res in pdb_get_residues(site):
            chains.add(res.chain.strip())
        sitename = '{}_{}'.format(sitename, '_'.join(chains))

    is_crosschain = len(sitename.split('_')) > 2
    hets, similarities, centralities = None, None, None
    with open(site, 'r') as f:
        for line in f:
            line = line.strip()
            if 'REMARK EC' in line:
                ec = line.split()[2]
            if 'REMARK NEARBY_HETS' in line:
                hets = literal_eval(''.join(line.split()[2:]))
            if 'REMARK HET_COGNATE_SIMILARITY' in line:
                similarities = literal_eval(''.join(line.split()[2:]))
            if 'REMARK MEAN_HET_CATRES_DISTANCE' in line:
                centralities = literal_eval(''.join(line.split()[2:]))

        if type(hets) != list:
            return

        info = []
        for het, sim, cent in zip(hets, similarities, centralities):
            if len(het) > 0:
                infostr = '{};{};{};{}'.format(het[0],het[1],sim,cent)
            info.append(infostr)

    return sitename, is_crosschain, ec, '"{}"'.format(info)

def get_similarity_scores():
    """Constructs a dictionary of pairwise similarity scores of
    active sites"""
    scores = {}
    #dataset = '../../datasets/sim_vs_rms.csv'
    dataset = '/Users/riziotis/ebi/phd/datasets/similarity_scores/raw/all_vs_all_catsites.pymol.csv'
    with open(dataset, 'r') as f:
        next(f)
        for line in csv.reader(f):
            ref = line[0]
            chain = line[1]
            #seq_sim = float(line[2])
            #cat_site_rmsd = float(line[3])
            #whole_chain_rmsd = float(line[4])
            cat_site_rmsd = float(line[2])
            seq_sim = None
            whole_chain_rmsd = None
            scores[(ref, chain)] = {'seq_sim': seq_sim, 
                                    'cat_site_rmsd': cat_site_rmsd,
                                    'whole_chain_rmsd': whole_chain_rmsd}
    return scores

if __name__ == '__main__':
    main()

