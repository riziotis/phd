#!/usr/bin/env python3

import sys
import os
import re
import csv
import warnings
import subprocess
from natsort import natsorted
from glob import glob
from Bio.PDB.MMCIFParser import MMCIFParser
from Bio.PDB.PDBIO import PDBIO, Select
from Bio import BiopythonWarning
from ..templates.superposition import tm_rms, charnley_rms, pymol_rms
warnings.simplefilter('ignore', BiopythonWarning)

WORKING_DIR = "/Users/riziotis/ebi/phd"
ASSEMBLIES_DIR = "{}/datasets/assembly_cif".format(WORKING_DIR) #Directory of mmCIF structure files are saved
SITES_DIR = "{}/results/stable/cat_sites".format(WORKING_DIR) #This is where all entries will be stored
RESULTS_DIR = '{}/results'.format(WORKING_DIR) #This is to store results

class ChainSelect(Select): 
    """to select pdb output"""

    def __init__(self, chains):
        super(ChainSelect, self).__init__()
        self.chains = tuple(chains)
 
    def accept_residue(self, residue):
        if residue.parent.id in self.chains:
            return 1
        else:
            return 0  


def get_ids(site):
    """Gets site ids"""
    id = site.split('/')[-1].split(".")[1].upper()
    pdb_id = id.split("_")[0]
    chains = get_chains(site)
    return id, pdb_id, chains


def get_chains(pdb):
    """Returns a list of unique chains ids of cat residues"""
    chains = []
    with open(pdb, 'r') as f:
        for line in f:
            line = line.strip()
            if line.startswith('ATOM'):
                chains.append(line[20:22].strip())
    return list(set(chains))


def ref_vs_all(entry, out):
    """Make comparisons of REFERENCE VS ALL HOMOLOGUES (whole sequence similarity,
    catalytic sites functional atoms RMSD, whole chain RMSD"""
    #TODO refactor this function
    try:
        ref = glob('{}/*reference*'.format(entry))[0]
        sites = glob('{}/*cat_site*.pdb'.format(entry))
    except IndexError:
        return
    #Get reference ids and structure
    ref_pdb_id = ref.split(".")[-3].upper()
    ref_chains = get_chains(ref)
    if len(ref_chains) > 2:
        return
    ref_id = '{}_{}'.format(ref_pdb_id, ref_chains[0])
    #Parse reference parent mmcif
    cif = glob('{}/{}-assembly-*.cif'.format(ASSEMBLIES_DIR, ref_pdb_id.lower()))[0]
    ref_structure = parser.get_structure('ref', cif)
    io.set_structure(ref_structure)
    io.save("{}/ref.temp".format(RESULTS_DIR), ChainSelect(ref_chains))
    #Do alignments of reference vs all homologues
    for site in sites:
        hom_id, hom_pdb_id, hom_chains = get_ids(site)
        if len(hom_chains) > 2:
            continue
        #Catalytic sites structural alignment
        try:
            cat_site_rmsd = float(charnley_rms(site, ref, reorder=True))
        except KeyboardInterrupt:
            exit()
        except:
            continue
        #Whole chain structural alignment
        try:
            cif = glob('{}/{}-assembly-*.cif'.format(ASSEMBLIES_DIR, hom_pdb_id.lower()))[0]
            hom_structure = parser.get_structure('hom', cif)
            io.set_structure(hom_structure)
            io.save("{}/hom.temp".format(RESULTS_DIR), ChainSelect(hom_chains))
            #Align with TM-align
            whole_rmsd = tm_rms('{}/ref.temp'.format(RESULTS_DIR), '{}/hom.temp'.format(RESULTS_DIR))
        except KeyboardInterrupt:
            exit()
        except:
            print(ref_id, hom_id, "alignment failed", file=sys.stderr)
            continue
        #Chain sequence similarity
        if (ref_id, hom_id) in sim_scores:
            similarity = sim_scores[(ref_id, hom_id)]
        elif (hom_id, ref_id) in sim_scores:
            similarity = sim_scores[(hom_id, ref_id)]
        else:
            continue
        #Print results in csv format
        print('{},{},{},{},{}'.format(ref_id, hom_id, similarity, 
            format(cat_site_rmsd, "5.3f"), 
            format(whole_rmsd, "5.3f")), 
            file = out)
    return


def all_vs_all(entry, out):
    try:
        sites = glob('{}/*.pdb'.format(entry))
        if len(sites) >= 500:
            sites = sites[:499]
    except IndexError:
        return
    #Do alignments of reference vs all homologues
    seen = set()
    for i in sites:
        i_id, i_pdb_id, i_chains = get_ids(i)
        for j in sites:
            j_id, j_pdb_id, j_chains = get_ids(j)
            if (j_id, i_id) in seen or i_id == j_id:
                continue
            #Catalytic sites structural alignment
            try:
                #cat_site_rmsd = float(charnley_rms(i, j, reorder=True))
                cat_site_rmsd = float(pymol_rms(i, j))
                seen.add((i_id, j_id))
                print('{},{},{}'.format(i_id, j_id, format(cat_site_rmsd, "5.3f")))
            except KeyboardInterrupt:
                exit()
            except:
                continue
    

def get_sequence_similarities(path):
    sim_scores = dict()
    with open(path, 'r') as f:
        next(f)
        for row in f:
            fields = row.split(',') 
            chA = fields[0].upper()
            chB = fields[1].upper()
            similarity = float(fields[2])
            sim_scores[(chA, chB)] = similarity
    return sim_scores

def main():

    #Load sequence similarity scores csv
    #csv_file = '/Users/riziotis/ebi/phd/datasets/alignment_scores_nr.csv'
    #sim_scores = get_sequence_similarities(csv_file)
    #Go to entries directory and get entries
    os.chdir(SITES_DIR)
    entries = natsorted(os.listdir(SITES_DIR))

    for entry in entries:
        if 'mcsa_0035' in entry or 'mcsa_0756' in entry:
            continue
        all_vs_all(entry, 'a')


main()
