warnings()
library(OneR) 
library(vioplot)
data <- read.csv("~/ebi/phd/datasets/sim_vs_rms/sim_vs_rms.jess.nofres.csv", 
                     header = T)


        ####PLOT ALL####

par(mfrow=c(2,1))
cor_coef <- cor(data$seq_similarity, data$cat_site_rmsd)
bin_names <- c('0-10','10-20','20-30','30-40','40-50','50-60','60-70','70-80','80-90','90-100')
data$seq_similarity_bined <- cut(data$seq_similarity, breaks = 10, labels = bin_names)
main <- 'Enzyme sequence similarity and\nCatalytic sites RMSD relationship'
xlabel <- '% sequence similarity'
ylabel <- 'Catalytic sites RMSD (Å)'
plot(data$seq_similarity*100, data$cat_site_rmsd, 
     xlim=c(0,100),
     ylim=c(0,5),
     cex=.06,
     main = main,
     xlab=xlabel,
     ylab=ylabel)
text(5, 4.8, paste('r =', format(cor_coef, digits=2)))
vioplot(cat_site_rmsd ~ seq_similarity_bined, data=data,
          #areaEqual=T, 
          varwidth=T,
          notch=F,
          range=100,
          xlim=c(0,11),
          ylim=c(0,5),
          xlab=xlabel,
          ylab=ylabel,
          col=terrain.colors(10))
text(1, 4.8, paste('r =', format(cor_coef, digits=2)))
        
        ####PLOT PER SITE SIZE####

#par(mfrow=c(3,4))
#xlabel <- '% sequence similarity'
#
#for (i in c(3,4,5,6,7,8,9,10,12,14)){
#  
#  if (i %in% c(3, 7, 12)){
#    ylabel='Catalytic sites RMSD (Å)'
#  }
#  else{
#    ylabel=''
#  }
#  #data.subset <- subset(data, nof_res==i)
#  #data.subset <- subset(data, nof_res==i & bound_a == T & bound_b == T)
#  #data.subset <- subset(data, nof_res==i & bound_a == F & bound_b == F)
#  #data.subset <- subset(data, nof_res==i & bound_a != bound_b)
#  
#          ####SCATTER PLOTS####
#  
#  x <- data.subset$seq_similarity
#  y <- data.subset$cat_site_rmsd
#  cor_coef <- cor(x, y)
##  
##  plot(x*100, y, 
##       xlim=c(0,100),
##       ylim=c(0,5),
##       cex=.06,
##       main=paste(i, 'residues'),
##       xlab=xlabel,
##       ylab=ylabel)
##  text(17, 4.5, paste('r =', format(cor_coef, digits=2)))
##}
#
#          ####BOX-VIOLIN PLOTS####
#
#  bin_names <- c('0-10','10-20','20-30','30-40','40-50','50-60','60-70','70-80','80-90','90-100')
#  data.subset$seq_similarity_bined <- cut(data.subset$seq_similarity, breaks = 10, labels = bin_names)
#  
#  boxplot(cat_site_rmsd ~ seq_similarity_bined,
#          data=data.subset,
#          #areaEqual=T, 
#          varwidth=T,
#          notch=F,
#          range=100,
#          wex=1.5,
#          main=paste(i, 'residues'),
#          xlim=c(0,11),
#          ylim=c(0,5),
#          xlab=xlabel,
#          ylab=ylabel,
#          col=terrain.colors(10))
#  text(5, 4.8, paste('r =', format(cor_coef, digits=2)))
#}
  