#!/usr/bin/env python3

import subprocess
import os
from glob import glob
from datetime import datetime

def main():
    """Submits multiple jobs to LSF"""

    now = datetime.now().strftime('%d-%m-%y')
    results_path = f'/nfs/research1/thornton/riziotis/research/phd/results/csa3d/seq_identities/{now}/data/'
    log_path = f'/nfs/research1/thornton/riziotis/research/phd/results/csa3d/seq_identities/{now}/logs/'
    for dir in (results_path, log_path):
        if not os.path.exists(dir):
            os.makedirs(dir)

    memory = 16000
    for infile in glob('/nfs/www-prod/web_hx2/research/thornton/atlas_site/alignments/align/out/*fasta'):
        outfile = os.path.join(results_path, '{}.seqid.csv'.format(infile.split('/')[-1].split('.')[0]))
        logfile = os.path.join(log_path, '{}.seqid.log'.format(infile.split('/')[-1].split('.')[0]))
        cmd = 'bsub -M {} -o {} python seq_identity.py {} {}'.format(memory, logfile, infile, outfile)
        subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

