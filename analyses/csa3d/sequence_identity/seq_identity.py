#!/usr/bin/env python3

import sys
import csv
from Bio import AlignIO

MD5_CHAINS = {}
with open('../../datasets/mcsa/similarity_scores/md5_to_chain.csv', 'r') as f:
    for i,l in enumerate(csv.reader(f)):
        md5 = l[0]
        chains = [c.strip() for c in l[1:]]
        MD5_CHAINS[md5] = chains
        
def main(msa_file, outfile):
    with open(outfile, 'w') as o:
        print('chain_A,chain_B,identity', file=o)
        try:
            alignment = AlignIO.read(msa_file, 'fasta')
        except ValueError:
            return
        seen = set()
        for p in alignment:
            for q in alignment:
                if (q.id, p.id) in seen or p.id == q.id:
                    continue
                seen.add((p.id, q.id))
                seqid = sequence_id(p.seq, q.seq)
                for p.chain in MD5_CHAINS[p.id]:
                    for q.chain in MD5_CHAINS[q.id]:
                        print('{},{},{}'.format(p.chain, q.chain, round(seqid, 3)), file=o)

def sequence_id(p, q):
    l = max(len(p.replace('-','')), len(q.replace('-','')))
    nof_aligned = 0
    for p_res, q_res in zip(p, q):
        if p_res != '-' and q_res != '-' and p_res == q_res:
            nof_aligned += 1
    return nof_aligned/l

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
