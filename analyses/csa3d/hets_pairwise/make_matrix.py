#!/usr/bin/env python3

import csv

HETCODES = set()
PARITY_SCORES = {}

# Get scores in dict
with open('./het_parity_scores.all.distances.csv', 'r') as f:
    next(f)
    for line in csv.reader(f):
        p_het = line[0]
        q_het = line[1]
        distance = line[3]

        HETCODES.add(p_het)
        HETCODES.add(q_het)

        PARITY_SCORES[(p_het, q_het)] = distance

# Make matrix
HETCODES = tuple(HETCODES)
for i in HETCODES:
    for j in HETCODES:
        try:
            distance = PARITY_SCORES[(i,j)]
        except KeyError:
            try:
                distance = PARITY_SCORES[(j,i)]
            except KeyError:
                distance = '0.5'
        if i==j:
            distance=0
        print(distance, end=',')
    print('')

