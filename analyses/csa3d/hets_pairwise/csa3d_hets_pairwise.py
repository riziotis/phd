#!/usr/bin/env python3

import csv
import parity_core as pc

HET_SMILES = dict()
with open('/Users/riziotis/ebi/phd/datasets/het_all_info.csv', 'r') as f:
    next(f)
    for het in csv.reader(f, quotechar='"'):
        hetcode = het[0]
        smiles = het[3]
        HET_SMILES[hetcode] = smiles

PARITY_SCORES = dict()
with open('/Users/riziotis/ebi/phd/datasets/csa3d/variation/data/all_vs_all.conc.nr.csv', 'r') as f:
    next(f)
    for i, pair in enumerate(csv.reader(f, quotechar='"')):
        p_hets = [i.split(';')[0] for i in pair[21].split(',')]
        q_hets = [i.split(';')[0] for i in pair[22].split(',')]

        for p_het in p_hets:
            try:
                p_smiles = HET_SMILES[p_het]
            except KeyError:
                continue
            for q_het in q_hets:
                try:
                    q_smiles = HET_SMILES[q_het]
                except KeyError:
                    continue
                parity_score = pc.generate_parity(p_smiles, q_smiles)
                if (p_het, q_het) in PARITY_SCORES or (q_het, p_het) in PARITY_SCORES:
                    continue
                PARITY_SCORES[(p_het, q_het)] = parity_score
        if i>200:
            break

with open('hets_parity_scores.csv', 'w') as o:
    print('hetcode_a,hetcode_b,parity_score',file=o)
    for k,v in PARITY_SCORES.items():
        print('{},{},{:.3f}'.format(k[0],k[1],v), file=o)


