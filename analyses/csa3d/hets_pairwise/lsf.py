#!/usr/bin/env python3

import subprocess
import os

def main():
    """Submits multiple jobs to LSF"""

    infiles_path = '/nfs/research1/thornton/riziotis/research/phd/datasets/csa3d/variation/data/per_entry/'
    results_path = '/nfs/research1/thornton/riziotis/research/phd/datasets/bound_ligands/parity_data/hets_parity_scores/per_entry/data/'
    log_path = '/nfs/research1/thornton/riziotis/research/phd/datasets/bound_ligands/parity_data/hets_parity_scores/per_entry/logs/'

    for dir in (infiles_path, results_path, log_path):
        if not os.path.exists(dir):
            os.makedirs(dir)

    memory = 16000
    for infile in os.listdir(infiles_path):
        outfile = os.path.join(results_path, '{}.hets_parity_scores.csv'.format(os.path.join(infile.split('.')[0])))
        logfile = os.path.join(log_path, '{}.hets_parity_scores.log'.format(os.path.join(infile.split('.')[0])))
        infile = os.path.join(infiles_path, infile)

        cmd = 'bsub -M {} -o {} python csa3d_hets_pairwise.py {} {}'.format(memory, logfile, infile, outfile)
        subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

