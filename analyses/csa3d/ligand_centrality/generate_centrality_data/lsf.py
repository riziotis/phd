#!/usr/bin/env python3

import subprocess
import os

def main():
    """Submits multiple jobs to LSF"""

    indir = '/nfs/research1/thornton/riziotis/research/phd/results/csa3d/entries/entries'
    outdir = f'/nfs/research1/thornton/riziotis/research/phd/results/csa3d/ligand_centrality/data/per_entry'
    logdir = f'/nfs/research1/thornton/riziotis/research/phd/results/csa3d/ligand_centrality/logs'

    # Big memory entries
    with open('/nfs/research1/thornton/riziotis/research/csa3d/tests/bigmem.list', 'r') as f:
        bigmem = set([int(entry.strip()) for entry in f.readlines()])
    
    for dir in (outdir, logdir):
        os.makedirs(dir, exist_ok=True)

    for infile in sorted(os.listdir(indir)):
        
        mcsa_id  = int(infile.split('.')[0].split('_')[1])

        memory = 15000
        if mcsa_id in bigmem:
            memory = 60000

        outfile = f"{infile.split('.')[0]}.csv"
        logfile = f"{infile.split('.')[0]}.log"
        
        infile = os.path.join(indir, infile)
        outfile = os.path.join(outdir, outfile)
        logfile = os.path.join(logdir, logfile)

        cmd = f"bsub -M {memory} -o {logfile} ./generate_centrality_data.py {infile} {outfile}"

        subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

