#!/usr/bin/env python3
import pandas as pd
import numpy as np
import pickle
import sys
from random import sample

def main(infile, outfile):

    # Parse M-CSA ID
    try:
        mcsa_id = int(infile.split('/')[-1].split('.')[0].split('_')[1])
    except IndexError:
        print('Something wrong with input filename')
        return
    # Load entry
    with open(infile, 'rb') as f:
        entry = pickle.load(f)
    # Limit to 500 sane sites per entry
    sites = [site for site in entry.pdbsites if site.is_sane]
    #if len(sites) > 500:
    #    sites = sample(sites, 500)

    # Initialize dict to store data
    data = {'mcsa_id': [],
            'site_id': [],
            'is_conserved': [],
            'dm_min': [],
            'dm_avg': [],
            'dm_com': [],
            'dm_cog': [],
            'hetcode': [],
            'hetid': [],
            'het_chain': [],
            'het_type': [],
            'het_similarity': [],
            'het_cannot_be_artefact': [],
            'het_components_missing': [],
            'het_dm_min': [],
            'het_dm_avg': [],
            'het_dm_com': [],
            'het_dm_cog': [],
            'het_c_min': [],
            'het_c_avg': [],
            'het_c_com': [],
            'het_c_cog': []}
    
    for site in sites:
        # Skip artefacts
        if not site.is_sane or len(site.ligands)==0:
            continue
        # Get mean distance using different definitions
        dm_min = mean_distance(site, 'min')
        dm_avg = mean_distance(site, 'avg')
        dm_com = mean_distance(site, 'com')
        dm_cog = mean_distance(site, 'cog')
        # Get het info
        for het in site.ligands:
            het_dm_min = het_mean_distance(site, het, 'min')
            het_dm_avg = het_mean_distance(site, het, 'avg')
            het_dm_com = het_mean_distance(site, het, 'com')
            het_dm_cog = het_mean_distance(site, het, 'cog')
            data['mcsa_id'].append(site.mcsa_id)
            data['site_id'].append(site.id)
            data['is_conserved'].append(site.is_conserved)
            data['dm_min'].append(dm_min)
            data['dm_avg'].append(dm_avg)
            data['dm_com'].append(dm_com)
            data['dm_cog'].append(dm_cog)
            data['hetcode'].append(het.resname)
            data['hetid'].append(het.resid)
            data['het_chain'].append(het.chain)
            data['het_type'].append(het.type)
            data['het_similarity'].append(het.similarity)
            data['het_cannot_be_artefact'].append(het.cannot_be_artefact)
            data['het_components_missing'].append(het.components_missing)
            data['het_dm_min'].append(het_dm_min)
            data['het_dm_avg'].append(het_dm_avg)
            data['het_dm_com'].append(het_dm_com)
            data['het_dm_cog'].append(het_dm_cog)
            data['het_c_min'].append(round((het_dm_min / dm_min),2))
            data['het_c_avg'].append(round((het_dm_avg / dm_avg),2))
            data['het_c_com'].append(round((het_dm_com / dm_com),2))
            data['het_c_cog'].append(round((het_dm_cog / dm_cog),2))
    # Convert to Pandas dataframe for easier data handling
    data = pd.DataFrame.from_dict(data)
    # Write csv
    with open(outfile, 'w') as o:
        data.to_csv(o, sep=',', index=False, line_terminator='\n')


def mean_distance(site, kind='avg'):
    """Calculates the mean inter-residue distance in a site"""
    dists = site.get_distances(kind)
    if dists.size > 0:
        try:
            return round(float(np.nanmean(dists)), 2)
        except TypeError:
            return
    return

def het_mean_distance(site, het, kind):
    """Calculates mean distance of a het to cat residues"""
    dists = [] 
    for res in site:
        dists.append(res.get_distance(het, kind))
    return round(float(np.nanmean(np.array(dists))), 2)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
