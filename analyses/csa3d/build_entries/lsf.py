#!/usr/bin/env python3

import subprocess
import os
from datetime import datetime

def main():
    """Submits multiple jobs to LSF"""

    now = datetime.now().strftime('%d-%m-%y')
    results_path = f'/nfs/research/thornton/riziotis/research/phd/results/csa3d/entries/{now}/entries/'
    log_path = f'/nfs/research/thornton/riziotis/research/phd/results/csa3d/entries/{now}/logs/'
    for dir in (results_path, log_path):
        if not os.path.exists(dir):
            os.makedirs(dir)

    # Big memory entries
    with open('/nfs/research/thornton/riziotis/research/csa3d/tests/bigmem.list', 'r') as f:
        bigmem = set([int(entry.strip()) for entry in f.readlines()])

    for i in range(970):
        if i==0:
            continue
        #if i not in (179, 205, 237, 246, 282, 362, 387, 396, 401):
        #   continue

        memory = 30000
        if i in bigmem:
            memory = 80000
            
        cmd = 'bsub -M {} -o {}/{}.log python build_entry.py {} {}'.format(memory, log_path, i, i, results_path)
        subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

