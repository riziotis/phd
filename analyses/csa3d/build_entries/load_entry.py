#!/usr/bin/env python3

import sys
import os
import pickle

def main(mcsa_id):
    with open('csa3d_{}.ent'.format(str(mcsa_id).zfill(4)), 'rb') as f:
        entry = pickle.load(f)

    outdir = './out/mcsa_{}'.format(str(mcsa_id).zfill(4))
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    for pdbsite in entry.pdbsites:
        try:
            rot, tran, rms, rms_all = pdbsite.reference_site.fit(pdbsite, transform=True)
            pdbsite.write_pdb(outdir=outdir, write_hets=True, func_atoms_only=False)
        except Exception as e:
            print(e)
            continue
    
if __name__ == '__main__':
    main(int(sys.argv[1]))

