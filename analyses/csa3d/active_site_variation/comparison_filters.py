"""Helper functions to filter active site comparisons based on ligands and/or function"""


def has_ligand(hets, sim_cutoff=0, ligand_type=None, exclude_artefacts=False, only_this=False):
    """We can specify the type of cognate ligand (any, substrate, cofactor, metal)
    Be careful with the 'substrate' flag: There is no per se annotation. 
    It is defined as a non-cofactor ligand."""
    if type(hets) == float:
        return False
    
    count = {'Substrate (non-polymer)': 0, 'Substrate (polymer)': 0,
             'Co-factor (non-ion)': 0, 'Co-factor (ion)': 0, 
             'Ion': 0, 'Artefact': 0, 'Any': 0}
    
    for het in hets.split(','):
        sim = het.split(';')[2]
        flag = het.split(';')[4]
        if sim == 'None':
            sim = sim_cutoff
        if float(sim) >= sim_cutoff:
            count['Any'] += 1
            count[flag] += 1

    if ligand_type is None: # Just check the presence of any ligand
        if exclude_artefacts:
            return (count['Any'] - count['Artefact']) > 0
        return count['Any'] > 0

    if count['Any']==0:
        return False
    
    if ligand_type == 'Any':
        if exclude_artefacts:
            if (count['Any'] - count['Artefact']) > 0:
                return True
            return False
        else:
            if count['Any']>0:
                return True
            return False

    if only_this:
        if count[ligand_type] == count['Any']:
            return True
        return False
    else:
        if count[ligand_type]>0:
            return True
        return False
            
def both_have_ligand(pair, sim_cutoff=0, ligand_type='Any', exclude_artefacts=False, only_this=False):
    """Checks if both sites have a cognate ligand bound"""
    return (has_ligand(pair['p_hets'], sim_cutoff, ligand_type, exclude_artefacts, only_this) and \
            has_ligand(pair['q_hets'], sim_cutoff, ligand_type, exclude_artefacts, only_this))

def one_has_ligand(pair, sim_cutoff=0, ligand_type='Any', exclude_artefacts=False, only_this=False):
    """Checks if one site has a cognate ligand and the other is ligand free"""
    return ((has_ligand(pair['p_hets'], sim_cutoff, ligand_type, exclude_artefacts, only_this) and \
            not has_ligand(pair['q_hets'], exclude_artefacts)) or \
            (has_ligand(pair['q_hets'], sim_cutoff, ligand_type, exclude_artefacts, only_this) and \
            not has_ligand(pair['p_hets'], exclude_artefacts)))

def both_free(pair, exclude_artefacts=False):
    """Checks if both sites are ligand-free"""
    return (not has_ligand(pair['p_hets'], exclude_artefacts) and not has_ligand(pair['q_hets'], exclude_artefacts))

def have_same_ligands(pair):
    """Checks if two sites share the same ligands"""
    if type(pair['p_hets']) == float or type(pair['q_hets']) == float:
        return False
    p_hets = pair['p_hets'].split(',')
    q_hets = pair['q_hets'].split(',')
    if len(p_hets) != len(q_hets):
        return False
    for p_het in p_hets:
        found = False
        for q_het in q_hets:
            if p_het.split(';')[0] == q_het.split(';')[0]:
                found = True
                break
        if not found:
            return False
    return True

def have_different_ligands(pair):
    """Checks if two sites have all their ligands different"""
    if type(pair['p_hets']) == float or type(pair['q_hets']) == float:
        return False
    p_hets = pair['p_hets'].split(',')
    q_hets = pair['q_hets'].split(',')
    for p_het in p_hets:
        for q_het in q_hets:
            if p_het.split(';')[0] == q_het.split(';')[0]:
                return False
    return True

def have_same_function(pair, level=None):
    """Checks if the ECs of the two sites match at the specified
    level. If level == None, then it compares all levels"""
    ec_a, ec_b = pair['p_ec'], pair['q_ec']
    if not level or type(ec_a) == float or type(ec_b) == float:
        return ec_a == ec_b
    else:
        ec_a = ec_a.split('.')
        ec_b = ec_b.split('.')
        return ec_a[:level] == ec_b[:level]
