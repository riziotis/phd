#!/usr/bin/env python3

import subprocess
import os
from natsort import natsorted
from datetime import datetime

def main():
    """Submits multiple jobs to LSF"""

    now = datetime.now().strftime('%d-%m-%y')
    entries_path = '/nfs/research1/thornton/riziotis/research/phd/datasets/csa3d/entries/res_2.0/entries/'
    comparisons_path = f'/nfs/research1/thornton/riziotis/research/phd/datasets/csa3d/variation/res_2.0/data/per_entry/' #REMOVE {now}
    output_path = f'/nfs/research1/thornton/riziotis/research/phd/results/csa3d/per_entry_analyses/{now}/results/'
    log_path = f'/nfs/research1/thornton/riziotis/research/phd/results/csa3d/per_entry_analyses/{now}/logs/'
    for dir in (output_path, log_path):
        if not os.path.exists(dir):
            os.makedirs(dir)

    # Big memory entries
    with open('/nfs/research1/thornton/riziotis/research/csa3d/tests/bigmem.list', 'r') as f:
        bigmem = set([int(entry.strip()) for entry in f.readlines()])

    for i in natsorted(os.listdir(entries_path)):
        i = i.split('.')[0]
        mcsa_id  = int(i.split('_')[1])
        
        memory = 30000
        if mcsa_id in bigmem:
            memory = 80000

        entry = os.path.join(entries_path, f'{i}.ent')
        comparisons = os.path.join(comparisons_path, f'{i}.csv')
        logfile = os.path.join(log_path, f'{i}.log')

        cmd = 'bsub -M {} -o {} python run_analyses.py {} {} {}'.format(memory, logfile, entry, comparisons, output_path)
        subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

