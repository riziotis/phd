#!/usr/bin/env python3
"""Script to generate all variation analysis plots for a CSA-3D entry"""

import sys
import os
import csv
import pickle 
import random
import pandas as pd
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
plt.rcParams['axes.axisbelow'] = True
import matplotlib.patches as mpatches
import seaborn as sns
import logomaker as lm
import rdkit
from collections import defaultdict, Counter
from glob import glob
from wordcloud import WordCloud
from Bio.PDB.DSSP import DSSP
from scipy.spatial.distance import squareform, pdist
from scipy.cluster.hierarchy import dendrogram, cut_tree
from sklearn import manifold
from unidip import UniDip
from pymol import cmd
from comparison_filters import *

WORKING_DIR = '/nfs/research1/thornton/riziotis/research/phd/'
if not os.path.exists(WORKING_DIR):
    WORKING_DIR = '/Users/riziotis/ebi/phd/'
ASSEMBLIES_DIR = WORKING_DIR + 'datasets/pdbe/assembly_cif/'
HETS_PARITY_CSV = WORKING_DIR + 'datasets/parity/csa3d.pairwise/csa3d_hets_parity_scores.all.csv'
DSSP_WILDCARD = WORKING_DIR + 'datasets/dssp/{}-assembly-*.dssp'

# Pairwise ligand PARITY scores dataset
with open(HETS_PARITY_CSV, 'r') as f:
    next(f)
    LIGDIST = {(l[1], l[2]): l[3] for l in csv.reader(f)}

def main(entry_path, comparisons_path, outdir):
    # Load entry
    with open(entry_path, 'rb') as entry:
        entry = pickle.load(entry)

    if len(entry.pdbsites) <= 1:
        print(f'Entry {entry.mcsa_id}: No homologues') 
        return

    # Load pairwise comparisons dataset
    with open(comparisons_path, 'r') as comparisons:
        comparisons = pd.read_csv(comparisons, low_memory=False)
    if comparisons.empty:
        print(f'Entry {entry.mcsa_id}: No pairwise comparison data') 
        return

    #REMOVE DAMN OUTLIER
    for site in entry.pdbsites:
        if '5js7' in site.pdb_id:
            entry.pdbsites.remove(site)
            break
    comparisons = comparisons.query('p_id != "5js7_A-A-A-A-A" and q_id != "5js7_A-A-A-A-A"')

    # Make output dir
    outdir = os.path.join(outdir, 'csa3d_{}'.format(str(entry.mcsa_id).zfill(4)))
    os.makedirs(outdir, exist_ok=True)

    conserved_only = False

    # Make plots
    try:
        write_structures(entry, outdir)
    except Exception as e:
        print('PlotError - Structures generation:',  e)
    try:
        plot_stats(entry, outdir)
    except Exception as e:
        print('PlotError - Stats plot:',e)
    try:
        plot_organisms(entry, outdir, conserved_only)
    except Exception as e:
        print('PlotError - Organisms plot:',e)
    try:
        plot_ecs(entry, outdir, conserved_only)
    except Exception as e:
        print('PlotError - EC plot:',e)
    try:
        plot_seqlogos(entry, outdir, conserved_only)
    except Exception as e:
        print('PlotError - Sequence logo plot:',e)
    try:
        plot_rmsds(entry, comparisons, outdir, conserved_only=True)
    except Exception as e:
        print('PlotError - Conserved RMSD plots:',e)
    try:
        plot_rmsds(entry, comparisons, outdir, conserved_only=False)
    except Exception as e:
        print('PlotError - RMSD plots:',e)
    try:
        get_rmsd_peaks(entry, comparisons, outdir)
    except Exception as e:
        print('PlotError - RMSD peak estimation:',e)
    try:
        get_per_res_rmsd_peaks(entry, comparisons, outdir)
    except Exception as e:
        print('PlotError - Per residue RMSD peak estimation:',e)
    try:
        plot_bfactors(entry, outdir, conserved_only)
    except Exception as e:
        print('PlotError - B-factors plot:',e)
    try:
        plot_hbonds(entry, outdir, conserved_only)
    except Exception as e:
        print('PlotError - H-bonds plot:',e)
    try:
        plot_sa(entry, outdir, conserved_only)
    except Exception as e:
        print('PlotError - Solvent accessibility plot:',e)
    try:
        plot_ss(entry, outdir, conserved_only)
    except Exception as e:
        print('PlotError - Secondary structure plot:',e)
    try:
        plot_comparisons(entry, comparisons, outdir, cognate_cutoff=0.6)
    except Exception as e:
        print('PlotError - Variation plot:',e)

    # Ligand analyses
    if len([l for s in entry.pdbsites for l in s.ligands]) == 0:
        print(f'Entry {entry.mcsa_id}: No ligands to plot') 
        return
    try:
        plot_ligand_contacts(entry, outdir)
    except Exception as e:
        print('PlotError - Ligand contacts plot:',e)
    try:
        plot_ligand_frequencies(entry, outdir)
    except Exception as e:
        print('PlotError - Ligand frequencies plot:',e)
    try:
        plot_ligand_clustering(entry, outdir)
    except Exception as e:
        print('PlotError - Ligand clustering plot:',e)

#    # Structural clustering
#    try:
#        plot_clustering(entry, comparisons, outdir)
#    except Exception as e:
#        print('PlotError - Clustering plot:',e)

# Data collection functions

def get_hbond_scores(entry, conserved_only=False):
    """Calculates the H-bond score of each catalytic residue using the Pymol API"""
    results = defaultdict(list)
    for site in entry.pdbsites:
        if conserved_only:
            if not site.is_conserved and not site.is_conservative_mutation:
                continue
        cmd.delete('all')
        parent_structure = glob(ASSEMBLIES_DIR + f'{site.pdb_id}*')[0]
        # Load structure in Pymol
        cmd.load(parent_structure, 'structure')
        # Iterate over catalytic residues
        for i, res in enumerate(site):
            hbond_score = np.nan
            if not res.is_gap:
                resid = res.auth_resid
                chain = res.chain
                resname = res.resname
                # Select acceptor/donor atoms of catalytic residue of interest
                sele1 = f'(structure and chain {chain} and resi {resid}) and (donors | acceptors)'
                # Select all other donors/acceptors in structure excluding solvent
                sele2 = f'(structure and not resi {resid}) and (donors | acceptors) and not solvent'
				# Get number of atoms in catalytic residue
                nofatoms = cmd.select('res', sele1)
                nofatoms = 1 if not nofatoms else nofatoms
                # Find atom pairs involved in H-bonds
                D = cmd.find_pairs(sele1, sele2, mode=1, cutoff=3.5)
                # Get number of bonds and calculate normalized score
                hbond_score = len(D)/nofatoms
            results[i].append(hbond_score)
    df = pd.DataFrame(results)
    df.columns = [''.join((r.resname, str(r.auth_resid), r.chain)) for r in entry.pdbsites[0].reference_site]
    return df

def get_ligand_clustering(entry):
    """Constructs dissimilarity matrix for all ligands bound in
    homologous family"""
    # Get ligands and annotations
    ligands = []
    seen = set()
    for site in entry.pdbsites:
        for l in site.ligands:
            if l.resname in seen:
                continue
            seen.add(l.resname)
            ligands.append(l)
    ligands_unique = list(set([' - '.join([l.resname, l.type]) for l in ligands]))
    if len(ligands_unique)>500:
        ligands_unique = random.sample(ligands_unique, 500)
    # Get pairwise PARITY scores
    dists = []
    seen = set()
    for p in ligands_unique:
        p = p.split(' - ')[0]
        for q in ligands_unique:
            q = q.split(' - ')[0]
            if p==q or (q,p) in seen:
                continue
            seen.add((p,q))
            try:
                dist = LIGDIST[(p,q)]
            except KeyError:
                try:
                    dist = LIGDIST[(q,p)]
                except KeyError:
                    dist = 0.5
            dists.append(dist)
    dists = np.array(dists, dtype='float32')
    # Make distance matrix
    matrix = squareform(dists)
    matrix = pd.DataFrame(matrix, columns=ligands_unique, index=ligands_unique)
    return matrix

def get_comparison_groups(data, cognate_cutoff=0.7):
    """Constructs comparisons groups (free-free, bound-bound, free-bound)"""
    cog = cognate_cutoff
    # Remove comparison entries where at least one EC is undefined
    cldata = data.query('not p_ec.isnull() & not q_ec.isnull()')
    # Keep only active sites with no residues missing (gaps)
    cldata = cldata[(cldata["p_sequence"].str.contains("_")==False) & (cldata["q_sequence"].str.contains("_")==False)]
    # Separate by UniProt mapping identity
    # Same UniProt ID 
    su = cldata.query('p_uniprot_id == q_uniprot_id')
    # Different UniProt ID
    du = cldata.query('p_uniprot_id != q_uniprot_id')
    # Separate by ligand presence
    su_ff = su[su.apply(both_free, axis=1)]
    su_ll = su[su.apply(both_have_ligand, sim_cutoff=cog, ligand_type='Any', only_this=False, axis=1)]
    su_lf = su[su.apply(one_has_ligand, sim_cutoff=cog, ligand_type='Any', only_this=False, axis=1)]
    su_sl = su[su.apply(have_same_ligands, axis=1)]
    su_dl = su[su.apply(have_different_ligands, axis=1)]
    du_ff = du[du.apply(both_free, axis=1)]
    du_ll = du[du.apply(both_have_ligand, sim_cutoff=cog, ligand_type='Any', only_this=False, axis=1)]
    du_lf = du[du.apply(one_has_ligand, sim_cutoff=cog, ligand_type='Any', only_this=False, axis=1)]
    du_sl = du[du.apply(have_same_ligands, axis=1)]
    du_dl = du[du.apply(have_different_ligands, axis=1)]
    # Make summary dataframe. 
    df = pd.DataFrame(columns=('rms', 'uniprot_id', 'ligands') )
    df = df_append(df, su_ff['wrms_all'], 'Same', 'Both ligand free')
    df = df_append(df, su_ll['wrms_all'], 'Same', 'Both bound with native ligand')
    df = df_append(df, su_lf['wrms_all'], 'Same', 'One free - one bound with native ligand')
    df = df_append(df, su_sl['wrms_all'], 'Same', 'Same ligands bound')
    df = df_append(df, su_dl['wrms_all'], 'Same', 'Different ligands bound')
    df = df_append(df, du_ll['wrms_all'], 'Different', 'Both bound with native ligand')
    df = df_append(df, du_ff['wrms_all'], 'Different', 'Both ligand free')
    df = df_append(df, du_lf['wrms_all'], 'Different', 'One free - one bound with native ligand')
    df = df_append(df, du_sl['wrms_all'], 'Different', 'Same ligands bound')
    df = df_append(df, du_dl['wrms_all'], 'Different', 'Different ligands bound')
    return df

def df_append(df1, df2, col1, col2):
    columns = ('rms', 'uniprot_id', 'ligands')
    temp = pd.DataFrame(columns=columns)
    temp.rms, temp.uniprot_id, temp.ligands = df2, col1, col2
    df = df1.append(temp)
    return df

def mds(matrix):
    """Performs multidimensional scaling on a distance matrix and returns
    point coordinates"""
    mds_model = manifold.MDS(n_components = 2, random_state = 2,
        dissimilarity = 'precomputed')
    mds_fit = mds_model.fit(matrix)  
    mds_coords = mds_model.fit_transform(matrix) 
    return mds_coords

def get_rmsd_matrix(entry, comparisons):
    """Constructs RMSD matrix of sites that are conserved or have conservative mutations"""
    ids = [p.id for p in entry.get_pdbsites() if p.is_sane and (p.is_conserved or p.is_conservative_mutation)]
    if len(ids)>500:
        ids = random.sample(ids, 500)
    rmsds = []
    for p,q in entry.all_vs_all(sane_only=True):
        if p.id in ids and q.id in ids:
            try:
                hit = comparisons[comparisons.isin([p.id]).any(axis=1) & comparisons.isin([q.id]).any(axis=1)]
                if not hit['wrms_all'].isnull().values.any():
                    rmsd = hit['wrms_all'].values[-1]
                else:
                    rmsd = hit['rms_all'].values[-1]
            except Exception as e:
                rmsd = 1
            rmsds.append(rmsd)
    rmsds = np.array(rmsds, dtype='float32')
    matrix = squareform(rmsds)
    matrix = pd.DataFrame(matrix, columns=ids, index=ids)
    return matrix

def get_ss(entry, conserved_only=False):
    """Get per-residue secondary structure of an entry in the form of a pseudo-sequence
    alignment"""
    ss_all = []
    ref = entry.pdbsites[0].reference_site
    for site in entry.pdbsites:
        if conserved_only:
            if not site.is_conserved and not site.is_conservative_mutation:
                continue
        ss = []
        if not site.is_sane:
            continue
        try:
            path = glob(DSSP_WILDCARD.format(site.pdb_id))[0]
            mdl = site.parent_structure[0]
            dssp = DSSP(mdl, path)
        except Exception:
            continue
        for res in site:
            try:
                k = (res.chain[0], (' ', res.auth_resid, ' '))
                ss.append(dssp[k][2])
            except Exception:
                ss.append('_')
        ss = ['C' if x=='_' else x for x in ss]
        ss = ''.join(ss)
        ss_all.append(ss)
    return ss_all

def rms_vs(entry, comparisons, outdir):
    """Residue solvent accessibility values from DSSP output vs RMSD"""
    results = defaultdict(list)
    comparisons = comparisons.loc[comparisons['mcsa_id']==entry.mcsa_id].query('p_is_reference or q_is_reference')
    for site in entry.pdbsites:

        b_min, b_max, b_mean, b_std = get_bfactor_stats(site)
        parent_structure = glob(ASSEMBLIES_DIR + f'{site.pdb_id}*')[0]
        cmd.delete('all')
        cmd.load(parent_structure, 'structure')
        if not site.is_sane:
            continue
        try:
            rmsds = np.array(comparisons.query('p_id == @site.id or q_id == @site.id')['per_res_rms'].values[-1].split(';'), dtype=float)
        except Exception:
            continue
        # Solvent accessibility
        try:
            path = glob(DSSP_WILDCARD.format(site.pdb_id))[0]
            mdl = site.parent_structure[0]
            dssp = DSSP(mdl, path)
        except Exception:
            continue
        # B-factors
        bs = [np.nanmean([normalize_01(a.get_bfactor(), b_min, b_max) for a in r.structure.get_atoms()]) if r.structure else np.nan for r in site]
        bs = np.array(bs, dtype='float')
        # H-bond scores
        hbond_scores = []
        for i, res in enumerate(site):
            hbond_score = np.nan
            if not res.is_gap:
                resid = res.auth_resid
                chain = res.chain
                resname = res.resname
                # Select acceptor/donor atoms of catalytic residue of interest
                sele1 = f'(structure and chain {chain} and resi {resid}) and (donors | acceptors)'
                # Select all other donors/acceptors in structure excluding solvent
                sele2 = f'(structure and not resi {resid}) and (donors | acceptors) and not solvent'
				# Get number of atoms in catalytic residue
                nofatoms = cmd.select('res', sele1)
                nofatoms = 1 if not nofatoms else nofatoms
                # Find atom pairs involved in H-bonds
                D = cmd.find_pairs(sele1, sele2, mode=1, cutoff=3.5)
                # Get number of bonds and calculate normalized score
                hbond_score = len(D)/nofatoms
            hbond_scores.append(hbond_score)
        # Ligand contacts
        ligand_contacts = []
        contacts = np.zeros(site.size, dtype='float32')
        for i, res in enumerate(site):
            if not res.structure:
                ligand_contacts.append(np.nan)
                continue
            contacts[i] = 0
            for het in site.ligands:
                hc = 0
                nofatoms = 0
                for ratom in res.structure.get_atoms():
                    for hatom in het.structure.get_atoms():
                        #TODO check me
                        nofatoms += 1
                        d = ratom - hatom
                        if d < 8:
                            hc += 1
                contacts[i] += hc/nofatoms

        # Summary
        for i, res in enumerate(site):
            rmsd = rmsds[i]
            bfactor = bs[i]
            hbond_score = hbond_scores[i]
            c = contacts[i]
            try:
                k = (res.chain[0], (' ', res.auth_resid, ' '))
                sa = float(dssp[k][3])
            except Exception:
                sa = np.nan

            results['mcsa_id'].append(site.mcsa_id)
            results['residue'].append('_'.join([str(i) for i in res.id]))
            results['rmsd'].append(rmsd)
            results['solvent_accessibility'].append(sa)
            results['bfactor'].append(bfactor)
            results['hbond_score'].append(hbond_score)
            results['ligand_contacts'].append(c)
    results = pd.DataFrame(results)
    results.set_index('residue')
    results.to_csv(os.path.join(outdir, 'rms_vs.csv'), index=False)
    return


def get_sa(entry, conserved_only=False):
    """Get per-residue solvent accessibility values from DSSP output"""
    sa_all = []
    ref = entry.pdbsites[0].reference_site
    for site in entry.pdbsites:
        if conserved_only:
            if not site.is_conserved and not site.is_conservative_mutation:
                continue
        key = []
        sa = []
        if not site.is_sane:
            continue
        try:
            path = glob(DSSP_WILDCARD.format(site.pdb_id))[0]
            mdl = site.parent_structure[0]
            dssp = DSSP(mdl, path)
        except Exception:
            continue
        for res in site:
            try:
                k = (res.chain[0], (' ', res.auth_resid, ' '))
                sa.append(float(dssp[k][3]))
            except Exception:
                sa.append(np.nan)
        sa_all.append(sa)
    sa_all = pd.DataFrame(sa_all)
    sa_all.columns = [''.join((r.resname, str(r.auth_resid), r.chain)) for r in entry.pdbsites[0].reference_site]
    return sa_all

def get_ligand_contacts(entry, conserved_only=False):
    """Returns a dataframe containing a per-residue ligand contact score for an entry.
    Score is the number of atom pairs with less than 8A distance, normalized over the total
    number of atoms of the two interacting residues"""
    contacts_all = []
    ref = entry.pdbsites[0].reference_site
    for site in entry.pdbsites:
        if conserved_only:
            if not site.is_conserved and not site.is_conservative_mutation:
                continue
        if len(site.ligands) == 0:
            continue
        contacts = np.zeros(site.size, dtype='float32')
        for i, res in enumerate(site):
            if not res.structure:
                continue
            contacts[i] = 0
            for het in site.ligands:
                hc = 0
                nofatoms = 0
                for ratom in res.structure.get_atoms():
                    for hatom in het.structure.get_atoms():
                        #TODO check me
                        nofatoms += 1
                        d = ratom - hatom
                        if d < 8:
                            hc += 1
                contacts[i] += hc/nofatoms
        contacts_all.append(contacts)
    contacts_all = np.array(contacts_all)
    contacts_all = np.concatenate(contacts_all).reshape(-1,ref.size)
    contacts_all = pd.DataFrame(contacts_all, columns = [''.join((r.resname, str(r.auth_resid), r.chain)) for r in ref])
    return contacts_all

def get_bfactors(entry, conserved_only=False):
    """Returns dataframe containing per residue average b-factors, normalized
    over the b-factors of the whole structure"""
    bfactors = []
    ref = entry.pdbsites[0].reference_site
    for site in entry.get_pdbsites():
        if conserved_only:
            if not site.is_conserved and not site.is_conservative_mutation:
                continue
        if not site.is_sane:
            continue
        b_min, b_max, b_mean, b_std = get_bfactor_stats(site)
        bs = [np.nanmean([normalize_01(a.get_bfactor(), b_min, b_max) for a in r.structure.get_atoms()]) if r.structure else np.nan for r in site]
        bs = np.array(bs, dtype='float')
        if not np.all(np.isnan(bs)):
            bfactors.append(bs)
    bfactors = np.array(bfactors)
    bfactors = np.concatenate(bfactors).reshape(-1,ref.size)
    bfactors = pd.DataFrame(bfactors, columns = [''.join((r.resname, str(r.auth_resid), r.chain)) for r in ref])
    return bfactors

def get_bfactor_stats(site):
    """Returns stats of the b-factors of the site's parent structure"""
    all_bfactors = []
    for c in site.parent_structure[0]:
        for r in c:
            for a in r:
                all_bfactors.append(a.get_bfactor())
    all_bfactors = np.array(all_bfactors)
    b_min = np.min(all_bfactors)
    b_max = np.max(all_bfactors)
    b_mean = np.mean(all_bfactors)
    b_std = np.std(all_bfactors)
    return b_min, b_max, b_mean, b_std

def normalize_01(v, v_min, v_max):
    """Normalize a value given a min and max value"""
    return (v-v_min)/(v_max-v_min)

def normalize_z(v, v_mean, v_std):
    """Normalize a value by calculating the z score"""
    return (v-v_mean)/v_std

def get_organisms(entry, conserved_only=False):
    """Returns a Counter object with the organisms of origin in the entry"""
    organisms = []
    for site in entry.pdbsites:
        if conserved_only:
            if not site.is_conserved and not site.is_conservative_mutation:
                continue
        if site.organism_name:
            organisms.append(site.organism_name.capitalize())
    organisms = Counter(organisms)
    return organisms

def get_ecs(entry, conserved_only=False):
    """Returns a dataframe with the EC number occupancies in the entry"""
    ecs = []
    for site in entry.pdbsites:
        if conserved_only:
            if not site.is_conserved and not site.is_conservative_mutation:
                continue
        if site.ec:
            ecs.append(site.ec)
    ecs = Counter(ecs)
    ecs = pd.DataFrame(dict(ecs), index=['count'])
    return ecs

def get_stats(entry):
    """Returns two dataframes containing basic stats about the entry, 
    one for PDB sites and one for UniProt sites"""
    pdb_stats = {}
    uni_stats = {}
    pdb_stats['All PDB sites'] = len([s for s in entry.pdbsites if s.is_sane])
    pdb_stats['Unique PDB structures'] = len(set([s.pdb_id for s in entry.pdbsites]))
    pdb_stats['PDB sites with same catalytic residues as reference'] = len([s for s in entry.pdbsites if s.is_conserved])
    pdb_stats['PDB sites with different catalytic residues from reference'] = len([s for s in entry.pdbsites if not s.is_conserved])
    pdb_stats['PDB sites with at least one chemically equivalent mutation, relative to the reference'] = len([s for s in entry.pdbsites if s.is_conservative_mutation])
    pdb_stats['Artificially mutated PDB sites'] = len([s for s in entry.pdbsites if (s.mapped_unisites and str(s)!=str(s.mapped_unisites[-1]))])
    pdb_stats['PDB sites mapped to UniProt sites'] = len([s for s in entry.pdbsites if s.mapped_unisites])
    uni_stats['All UniProt sites'] = len(entry.unisites)
    uni_stats['UniProt sites with same catalytic residues as reference'] = len([s for s in entry.unisites if s.is_conserved])
    uni_stats['UniProt sites with different catalytic residues from reference'] = len([s for s in entry.unisites if not s.is_conserved])
    uni_stats['UniProt sites with at least one chemically equivalent mutation, relative to the reference'] = len([s for s in entry.unisites if s.is_conservative_mutation])
    uni_stats['UniProt sites mapped to PDB sites'] = len([s for s in entry.unisites if s.mapped_pdbsites])
    pdb_stats = pd.DataFrame(pdb_stats, index=['Number of sites'])
    uni_stats = pd.DataFrame(uni_stats, index=['Number of sites'])
    return pdb_stats, uni_stats

def get_rmsd_peaks(entry, comparisons, outdir):
    """Performs Hartigan's dip test on RMSD distributions to estimate the number of peaks."""
    all_vs_all = comparisons
    ref_vs_all = comparisons.query('p_is_reference or q_is_reference') 
    aa = np.array(all_vs_all['wrms_all'].sort_values())
    ra = np.array(ref_vs_all['wrms_all'].sort_values())
    aa = aa[~np.isnan(aa)]
    ra = ra[~np.isnan(ra)]
    aa_intervals = UniDip(aa, alpha=0.03).run()
    ra_intervals = UniDip(ra, alpha=0.03).run()
    results = {'mcsa_id': [entry.mcsa_id], 'all_vs_all_peaks': [len(aa_intervals)], 'ref_vs_all_peaks': [len(ra_intervals)]}
    results = pd.DataFrame(results)
    results.to_csv(os.path.join(outdir, 'rmsd_peaks.csv'), index=False)
    return

def get_per_res_rmsd_peaks(entry, comparisons, outdir):
    """Performs Hartigan's dip test on per-residue RMSD distributions to estimate the number of peaks."""
    all_vs_all = comparisons
    ref_vs_all = comparisons.query('p_is_reference or q_is_reference') 
    aa = all_vs_all['per_res_rms'].str.split(';', expand=True)
    ra = ref_vs_all['per_res_rms'].str.split(';', expand=True)
    results = {}
    results['mcsa_id'] = [entry.mcsa_id]
    for name, data in zip(('all_vs_all_per_res_rms_peaks', 'ref_vs_all_per_res_rms_peaks'), (aa, ra)):
        peaks = []
        for res in data:
            rmsds = np.array(data[res].sort_values().astype('float32'))
            rmsds = rmsds[~np.isnan(rmsds)]
            intervals = UniDip(rmsds, alpha=0.02).run()
            peaks.append(len(intervals))
        results[name] = [';'.join([str(i) for i in peaks])]
    results = pd.DataFrame(results)
    results.to_csv(os.path.join(outdir, 'per_res_rmsd_peaks.csv'), index=False)
    return


# Plotting functions

def plot_stats(entry, outdir):
    """Basic stats"""
    # Get data
    pdb_stats, uni_stats = get_stats(entry)
    # Make barplot
    pdb_colors = ['#B22222' for i in pdb_stats.columns]
    uni_colors = ['#FFD700' for i in uni_stats.columns]
    fig, axs = plt.subplots(2, 1, figsize=(15,15))
    for data, color, ax in zip ((pdb_stats, uni_stats), (pdb_colors, uni_colors), axs):
        st = sns.barplot(data=data, linewidth=2, edgecolor='.2', orient='h', ax=ax, palette=color)
        ax.grid(True, axis='x') 
        ax.set_xlabel('Count', fontsize=20)
        ax.tick_params(axis='x', labelsize=16)
        ax.tick_params(axis='y', labelsize=16)
    fig.suptitle('M-CSA entry {}: Statistics of homologous active sites'.format(entry.mcsa_id), fontsize=24)
    axs[0].set_title('Active sites with 3D information (PDB sites)', fontsize=20)
    axs[1].set_title('Active sites without 3D information (UniProt catalytic residue sets)', fontsize=20)
    plt.savefig(os.path.join(outdir, 'stats.png'), bbox_inches='tight')
    df = pd.concat([pdb_stats,uni_stats], axis=1).rename(index={'Number of sites':entry.mcsa_id}).rename_axis(index='M-CSA ID')
    df.to_csv(os.path.join(outdir, 'stats.csv'))
    return

def plot_organisms(entry, outdir, conserved_only=False):
    """Organisms word cloud"""
    # Get organisms and ecs
    organisms = get_organisms(entry, conserved_only)
    #  Plot
    wc = WordCloud(width=1600, height=800, background_color="white", repeat=False)
    wc.generate_from_frequencies(organisms)
    fig, ax = plt.subplots(figsize=(15,10))
    fig.tight_layout()
    ax.set_title('Word cloud of organisms of origin', fontsize=24)
    ax.tick_params(axis='both', labelbottom=False, labelleft=False)
    ax.imshow(wc)
    plt.savefig(os.path.join(outdir, 'organisms.png'))
    df = pd.DataFrame.from_dict(organisms, orient='index').reset_index().rename(columns={'index':'organism', 0:'count'})
    df.to_csv(os.path.join(outdir, 'organisms.csv'), index=False)
    return

def plot_ecs(entry, outdir, conserved_only=False):
    """EC frequencies"""
    # Get ECs and plot
    ecs = get_ecs(entry, conserved_only)
    fig, ax = plt.subplots(figsize=(10,8))
    bar = sns.barplot(data=ecs, edgecolor='black', ax=ax)
    ax.set_title('E.C. numbers in family', fontsize=24)
    ax.set_xlabel('E.C. number', fontsize=20)
    ax.grid(axis='y')
    ax.set_ylabel('Count', fontsize=20)
    ax.tick_params(axis='both', labelsize=12)
    bar.set_xticklabels(bar.get_xticklabels(), rotation=20, horizontalalignment='right')
    plt.savefig(os.path.join(outdir, 'ecs.png'))
    ecs.T.rename_axis(index='ec').to_csv(os.path.join(outdir, 'ecs.csv'))
    return

def plot_seqlogos(entry, outdir, conserved_only=False):
    """Sequence Logos"""
    # Generate pseudo-sequence alignments
    pdb_seqs = []
    uni_seqs = []
    for site in entry.pdbsites:
        if conserved_only:
            if not site.is_conserved and not site.is_conservative_mutation:
                continue
        pdb_seqs.append(str(site))
    for site in entry.unisites:
        if conserved_only:
            if not site.is_conserved and not site.is_conservative_mutation:
                continue
        uni_seqs.append(str(site))
    ref_labels = [''.join((r.resname, str(r.auth_resid), r.chain)) for r in entry.pdbsites[0].reference_site]
    # Plot
    fig, axs = plt.subplots(1, 2, figsize=(20,8))
    fig.suptitle('Pseudo-sequence logos of homologous active sites', fontsize=24)
    fig.tight_layout(pad=2)
    for aln, ax in zip((pdb_seqs, uni_seqs), axs):
        mat = lm.alignment_to_matrix(aln, pseudocount=0, to_type='information', characters_to_ignore='_-X')
        logo = lm.Logo(mat, ax=ax, color_scheme='weblogo_protein')
        ax.grid(False)
        ax.set_xlabel('Reference catalytic residue', fontsize=16)
        ax.set_ylabel('Bins', fontsize=16)
        ax.tick_params(axis='both', labelsize=18)
        ax.set_xticks(range(len(ref_labels)))
        ax.set_xticklabels(ref_labels)
        axs[0].set_title('PDB active sites', fontsize=20)
        axs[1].set_title('UniProt active sites', fontsize=20)
    plt.savefig(os.path.join(outdir, 'seqlogos.png'), bbox_inches='tight', transparent=True)
    return

def write_structures(entry, outdir):
    """Write PDB files for 3D visualization"""
    # First superimpose everything to the reference and write PDB structures
    d = os.path.join(outdir, 'structures')
    os.makedirs(d, exist_ok=True)
    for site in entry.pdbsites:
        if not site.is_sane:
            continue
        try:
            site.reference_site.fit(site, weighted=True, transform=True)
        except Exception:
            try:
                site.reference_site.fit(site, weighted=False, transform=True, cycles=10, cutoff=5)
            except Exception as e:
                print('Structures generation warning:', e)
                continue
        site.write_pdb(outdir=d, write_hets=True, func_atoms_only=False)
    return

def plot_rmsds(entry, comparisons, outdir, conserved_only=False):
    """All vs. All RMSD over all functional atoms and per residue RMSD distributions"""
    # Generate RMSD data
    all_vs_all = comparisons
    if conserved_only:
        all_vs_all = all_vs_all.query('(p_is_conserved or p_is_conservative_mutation) and (q_is_conserved or q_is_conservative_mutation)')
    ref_vs_all = all_vs_all.query('p_is_reference or q_is_reference') 
    ref = entry.pdbsites[0].reference_site
    bins = [i for i in np.arange(0,10,0.5)] + [50.0]
    xticks = [i for i in np.arange(0,10.5,0.5)]
    xticklabels = [str(i) for i in np.arange(0,10,0.5)] + ['≥10']
    yticks = [i for i in np.arange(0,10.5,1)]
    yticklabels = [str(i) for i in np.arange(0,10,1)] + ['≥10']

    nofhoms = len(entry.pdbsites)
    if conserved_only:
        nofhoms = len([s for s in entry.pdbsites if s.is_conserved or s.is_conservative_mutation])

    # All-vs-All and Ref-vs-All RMSD distributions
    for dataset, name in zip((all_vs_all, ref_vs_all), ('all_vs_all', 'ref_vs_all')):
        # Both conserved and mutated
        fig, ax = plt.subplots(figsize=(10,8))
        fig.suptitle('M-CSA {}'.format(entry.mcsa_id), fontsize=24)
        hist = sns.histplot(data=dataset['wrms_all'], edgecolor='black', kde=True, ax=ax, bins=bins)
        ax.set_title('{} {}homologues\n{}'.format(nofhoms, 'conserved ' if conserved_only else '', entry.info['enzyme_name']), fontsize=16)
        ax.set_xlim([0,10.5])
        ax.set_xlabel('RMSD (Å)', fontsize=16)
        ax.set_xticks(xticks)
        ax.set_xticklabels(xticklabels, rotation=35)
        ax.set_ylabel('Count', fontsize=16)
        ax.grid(axis='y')
        ax.tick_params(axis='both', labelsize=18)
        plt.savefig(os.path.join(outdir, 'csa3d_{}.{}.{}rmsds.png'.format(str(entry.mcsa_id).zfill(4), name, 'conserved.' if conserved_only else '')), bbox_inches='tight', facecolor='white')

        # Per residue RMSD distibutions
        # Tidy up data
        columns = [''.join((r.resname, str(r.auth_resid), r.chain)) for r in ref]
        per_res = dataset.loc[~pd.isnull(dataset['per_res_rms']), 'per_res_rms'].astype('str').str.split(';', expand=True).astype('float32')
        per_res[per_res>10]=10 # Just and upper limit bin to hold all large RMSD values
        per_res.columns = columns
        fig, ax = plt.subplots(figsize=(10,8))
        fig.suptitle('M-CSA {}'.format(entry.mcsa_id), fontsize=24)
        sns.violinplot(data=per_res, cut=0, ax=ax)
        sns.despine(bottom=True)
        ax.set_title('{} {}homologues\n{}'.format(nofhoms, 'conserved ' if conserved_only else '',  entry.info['enzyme_name']), fontsize=16)
        ax.set_ylabel('RMSD (Å)', fontsize=16)
        ax.set_xlabel('Reference catalytic residue', fontsize=16, labelpad=10)
        ax.set_xticklabels(ax.get_xticklabels(), rotation=20, horizontalalignment='right')
        ax.set_ylim([0,10.5])
        ax.set_yticks(yticks)
        ax.set_yticklabels(yticklabels)
        ax.grid(axis='y')
        ax.tick_params(axis='both', labelsize=18)
        sns.stripplot(data=per_res, ax=ax, color='black', size=2.1, alpha=0.25) # original size 1.8, alphda 0.15
        plt.savefig(os.path.join(outdir, 'csa3d_{}.{}.{}per_res_rmsds.png'.format(str(entry.mcsa_id).zfill(4), name, 'conserved.' if conserved_only else '')), bbox_inches='tight', facecolor='white')
    return

def plot_bfactors(entry, outdir, conserved_only=False):
    """Per-residue average B-factor"""
    # Calculate B-factors
    bfactors = get_bfactors(entry, conserved_only)
    # Plot
    fig, ax = plt.subplots(figsize=(10,6))
    sns.violinplot(data=bfactors, ax=ax)
    sns.stripplot(data=bfactors, ax=ax, color='black', size=2.1, alpha=0.25)
    sns.despine(trim=True, bottom=True)
    ax.set_title('Per-residue average B-factor distributions', fontsize=24)
    ax.set_xlabel('Reference catalytic residue', fontsize=16, labelpad=10)
    ax.set_ylabel('Normalized B-factor', fontsize=16)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=20, ha='right')
    ax.tick_params(axis='both', labelsize=18)
    ax.grid(axis='y')
    plt.savefig(os.path.join(outdir, 'bfactors.png'), bbox_inches='tight', transparent=True)
    bfactors.to_csv(os.path.join(outdir, 'bfactors.csv'), index=False)
    return

def plot_hbonds(entry, outdir, conserved_only=False):
    """Per-residue hydrogen bonding"""
    # Calculate B-factors
    hbonds = get_hbond_scores(entry, conserved_only)
    # Plot
    fig, ax = plt.subplots(figsize=(10,6))
    sns.violinplot(data=hbonds, ax=ax)
    sns.stripplot(data=hbonds, ax=ax, color='black', size=2.1, alpha=0.25)
    sns.despine(trim=True, bottom=True)
    ax.set_title('Per-residue hydrogen bonding', fontsize=24)
    ax.set_xlabel('Reference catalytic residue', fontsize=16, labelpad=10)
    ax.set_ylabel('H-bond score', fontsize=16)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=20, ha='right')
    ax.tick_params(axis='both', labelsize=18)
    ax.grid(axis='y')
    plt.savefig(os.path.join(outdir, 'hbonds.png'), bbox_inches='tight', transparent=True)
    hbonds.to_csv(os.path.join(outdir, 'hbonds.csv'), index=False)
    return

def plot_sa(entry, outdir, conserved_only=False):
    """Per-residue solvent accessibility"""
    # Calculate B-factors
    sa = get_sa(entry, conserved_only)
    # Plot
    fig, ax = plt.subplots(figsize=(10,6))
    sns.violinplot(data=sa, ax=ax)
    sns.stripplot(data=sa, ax=ax, color='black', size=2.1, alpha=0.25)
    sns.despine(trim=True, bottom=True)
    ax.set_title('Per-residue solvent accessibility', fontsize=24)
    ax.set_xlabel('Reference catalytic residue', fontsize=16, labelpad=10)
    ax.set_ylabel('Accessible Surface Area', fontsize=16)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=20, ha='right')
    ax.tick_params(axis='both', labelsize=18)
    ax.grid(axis='y')
    plt.savefig(os.path.join(outdir, 'solvent_accessibility.png'), bbox_inches='tight', transparent=True)
    sa.to_csv(os.path.join(outdir, 'solvent_accessibility.csv'), index=False)
    return

def plot_ligand_contacts(entry, outdir, conserved_only=False):
    """Per-residue number of contacts with ligands"""
    # Calculate contacts
    contacts_all = get_ligand_contacts(entry, conserved_only)
    # Plot
    fig, ax = plt.subplots(figsize=(10,6))
    sns.violinplot(data=contacts_all, ax=ax)
    sns.stripplot(data=contacts_all, ax=ax, color='black', size=2.1, alpha=0.25)
    sns.despine(trim=True, bottom=True);
    ax.set_title('Per-residue ligand contact score distributions', fontsize=24)
    ax.set_xlabel('Reference catalytic residue', fontsize=16, labelpad=10)
    ax.set_ylabel('Normalized number of contacts', fontsize=16)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=20, ha='right')
    ax.tick_params(axis='both', labelsize=18)
    ax.grid(axis='y')
    plt.savefig(os.path.join(outdir, 'lig_contacts.png'), bbox_inches='tight', transparent=True)
    contacts_all.to_csv(os.path.join(outdir, 'ligand_contacts.csv'), index=False)
    return

def plot_ss(entry, outdir, conserved_only=False):
    """Secondary structure logo"""
    # Run DSSP to get secondary structure assignments for each catalytic residue
    ss_all = get_ss(entry, conserved_only)
    ref_labels = [''.join((r.resname, str(r.auth_resid), r.chain)) for r in entry.pdbsites[0].reference_site]
    # Plot
    fig, ax = plt.subplots(figsize=(10,6))
    mat = lm.alignment_to_matrix(ss_all, pseudocount=0, to_type='probability', characters_to_ignore='_-')
    logo = lm.Logo(mat, ax=ax, color_scheme='weblogo_protein')
    ax.grid(False)
    ax.set_title('Secondary structure logo', fontsize=24)
    ax.set_xlabel('Reference catalytic residue', fontsize=16)
    ax.set_ylabel('Frequency', fontsize=16)
    ax.set_xticks(range(len(mat)))
    ax.set_xticklabels(ref_labels, rotation=20, ha='right')
    ax.tick_params(axis='both', labelsize=18)
    plt.savefig(os.path.join(outdir, 'sec_structure.png'), bbox_inches='tight', transparent=True)
    return

def plot_clustering(entry, comparisons, outdir):
    """Clustering heatmap and dendrogram for templates"""
    # Build RMSD matrix for conserved sites
    matrix = get_rmsd_matrix(entry, comparisons)
    if matrix.empty:
        print(f'Entry {entry.mcsa_id}: No sane conserved sites to perform clustering on')
        return
    matrix.to_csv(os.path.join(outdir, 'rmsd_matrix.conserved.csv'), index=True)
    ids = list(matrix.index.copy())
    # Clustered heatmap 
    sns.set_theme(style='white')
    cmap = sns.color_palette("YlOrBr_r", as_cmap=True)
    heatmap = sns.clustermap(matrix, cmap=cmap, cbar_kws={'label': 'RMSD'}, figsize=(20,20))
    plt.savefig(os.path.join(outdir, 'clustermap.png'))
    # Dendrogram and MDS map
    # Reorder site ids
    reordered_ids = [ids[i] for i in heatmap.dendrogram_row.reordered_ind]
    # Dendrogramp plot
    fig, axs = plt.subplots(2, 1, figsize=(15,20))
    x = mds(matrix)
    Z = heatmap.dendrogram_col.linkage
    den = dendrogram(Z, labels=ids, color_threshold=0.4*max(Z[:,2]), above_threshold_color='black', ax=axs[0])
    axs[0].tick_params(axis='x', labelsize=12)
    axs[0].tick_params(axis='y', labelsize=12)
    axs[0].set_ylabel('RMSD (Å)', fontsize=16)
    axs[0].grid(axis='y')
    # Colored MDS plot
    points = den['leaves']
    colors = ['none'] * len(points)
    for xs, c in zip(den['icoord'], den['color_list']):
        for xi in xs:
            if xi % 10 == 5:
                colors[(int(xi)-5) // 10] = c
    for point, color, label in zip(points, colors, reordered_ids):
        axs[1].scatter(x[point, 0], x[point, 1], color=color, edgecolor='black', s=80)
    axs[1].set_xlabel('First dimension', fontsize=16)
    axs[1].set_ylabel('Second dimension', fontsize=16)
    axs[1].grid()
    plt.savefig(os.path.join(outdir, 'dendrogram.png'))
    return

def plot_comparisons(entry, comparisons, outdir, cognate_cutoff=0.7):
    """RMSD distributions based on ligand presence"""
    # Get groups
    df = get_comparison_groups(comparisons, cognate_cutoff)
    sns.set_theme(context='talk', style='whitegrid', font_scale=0.9)
    fig, axs = plt.subplots(1, 2, figsize=(15,6))
    # Same Uniprot mapping
    same = df.query('uniprot_id == "Same"')
    diff = df.query('uniprot_id == "Different"')
    for ax, data in zip(axs, (same, diff)):
        try:
            boxplot = sns.boxplot(data=data, x='ligands', y='rms', palette='muted', ax=ax)
            boxplot.set(xlabel=None)
            boxplot.set_xticklabels(boxplot.get_xticklabels(), rotation=20, horizontalalignment='right')
        except ValueError:
            pass
        ax.set_ylabel('RMSD (Å)', fontsize=17)
    axs[0].set_title('Active sites from identical sequences', fontsize=20)
    axs[1].set_title('Active sites from different sequences', fontsize=20)
    plt.savefig(os.path.join(outdir, 'comparisons.png'), bbox_inches='tight')
    return 

def plot_ligand_frequencies(entry, outdir):
    """Frequencies of ligands bound in homologous family"""
    ligands = []
    for site in entry.pdbsites:
        for ligand in site.ligands:
            ligands.append(ligand.resname)
    # count words
    counts = Counter(ligands)
    labels, values = zip(*counts.items())
    # sort values in descending order
    indSort = np.argsort(values)[::-1]
    # rearrange data
    labels = np.array(labels)[indSort]
    values = np.array(values)[indSort]
    # make barplot
    fix, ax = plt.subplots(figsize=(15,10))
    indeces = np.arange(len(labels))
    bar = sns.barplot(x=indeces, y=values, ax=ax, palette='flare')
    # add labels
    ax.set_title('Ligands bound in homologous structures', fontsize=20)
    ax.set_xlabel('Ligand', fontsize=16)
    ax.set_ylabel('Count', fontsize=16)
    ax.set_xticklabels(labels, rotation=45)
    plt.savefig(os.path.join(outdir, 'ligand_freqs.png'))
    return

def plot_ligand_clustering(entry, outdir):
    """Clustering of ligands based on their chemical similarity 
    (PARITY score)"""
    # Get dissimilarity matrix and MDS map
    matrix = get_ligand_clustering(entry) 
    if matrix.empty:
        print(f'Entry {entry.mcsa_id}: No ligands to perform clustering on')
        return
    mds_coords = mds(matrix)
    # Plot heatmap
    cmap = sns.color_palette('light:b_r', as_cmap=True)
    heatmap = sns.clustermap(matrix, cmap=cmap, cbar_kws={'label': 'Dissimilarity'}, 
                             figsize=(15,12), xticklabels=False)
    heatmap.ax_row_dendrogram.set_visible(False)
    plt.savefig(os.path.join(outdir, 'ligands_clustermap.png'))
    # Plot MDS
    fig, ax = plt.subplots(figsize=(15,10))
    sns.set_style("whitegrid")
    ax.scatter(mds_coords[:,0],mds_coords[:,1],
        facecolors = 'none', edgecolors = 'none')  # points in white (invisible)
    labels = [l.split(' - ')[0] for l in matrix.index]
    for label, x, y in zip(labels, mds_coords[:,0], mds_coords[:,1]):
        plt.annotate(label, (x,y), xycoords = 'data')
    plt.xlabel('First Dimension', fontsize=16)
    plt.ylabel('Second Dimension', fontsize=16)
    plt.title('Dissimilarity among ligands', fontsize=20)    
    plt.savefig(os.path.join(outdir, 'ligands_mds.png'))
    return

if __name__ == '__main__':
   main(sys.argv[1], sys.argv[2], sys.argv[3]) 
