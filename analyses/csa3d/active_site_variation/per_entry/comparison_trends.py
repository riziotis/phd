#!/usr/bin/env python3
"""Script to get the mean RMSD values of the apo-apo, holo-holo, apo-holo comparison pairs """

import sys
import os
import csv
import pickle 
import pandas as pd
import numpy as np
from collections import defaultdict, Counter
from glob import glob
from natsort import natsorted
from comparison_filters import *

WORKING_DIR = '/nfs/research1/thornton/riziotis/research/phd/'
if not os.path.exists(WORKING_DIR):
    WORKING_DIR = '/Users/riziotis/ebi/phd/'
COMPARISONS_WILDCARD = WORKING_DIR + 'datasets/csa3d/variation/data/per_entry/csa3d_*.csv'

def main():
    results = []
    for i, entry in enumerate(natsorted(glob(COMPARISONS_WILDCARD))):
        # Load pairwise comparisons dataset
        with open(entry, 'r') as comparisons:
            comparisons = pd.read_csv(comparisons, low_memory=False)
        # TODO Check if we want to generate the simple plots first
        if comparisons.empty:
            print(f'{entry}: No pairwise comparison data') 
            continue

        results.append(get_comparison_groups(comparisons))

    results = pd.DataFrame(results)
    results.to_csv('/nfs/research1/thornton/riziotis/research/phd/results/csa3d/per_entry_analyses/comparison_trends.py', index=False)


def get_comparison_groups(data, cognate_cutoff=0.6):
    """Constructs comparisons groups (free-free, bound-bound, free-bound)"""
    cog = cognate_cutoff
    # Remove comparison entries where at least one EC is undefined
    cldata = data.query('not p_ec.isnull() & not q_ec.isnull()')
    # Keep only active sites with no residues missing (gaps)
    cldata = cldata[(cldata["p_sequence"].str.contains("_")==False) & (cldata["q_sequence"].str.contains("_")==False)]
    # Separate by UniProt mapping identity
    # Same UniProt ID 
    su = cldata.query('p_uniprot_id == q_uniprot_id')
    # Different UniProt ID
    du = cldata.query('p_uniprot_id != q_uniprot_id')
    # Separate by ligand presence
    results = {}
    results['mcsa_id'] = data.iloc[0]['mcsa_id']
    results['nof_su'] = su.drop_duplicates(subset='p_id').shape[0]
    results['nof_du'] = du.drop_duplicates(subset='p_id').shape[0]
    results['su_ff'] = su[su.apply(both_free, axis=1)]['wrms_all'].median()
    results['su_ll'] = su[su.apply(both_have_ligand, sim_cutoff=cog, ligand_type='Any', only_this=False, axis=1)]['wrms_all'].median()
    results['su_lf'] = su[su.apply(one_has_ligand, sim_cutoff=cog, ligand_type='Any', only_this=False, axis=1)]['wrms_all'].median()
    results['du_ff'] = du[du.apply(both_free, axis=1)]['wrms_all'].median()
    results['du_ll'] = du[du.apply(both_have_ligand, sim_cutoff=cog, ligand_type='Any', only_this=False, axis=1)]['wrms_all'].median()
    results['du_lf'] = du[du.apply(one_has_ligand, sim_cutoff=cog, ligand_type='Any', only_this=False, axis=1)]['wrms_all'].median()
    
    if all(v is not np.nan for v in (results['su_ff'], results['su_ll'], results['su_lf'])):
        results['su_expected_trend'] = (results['su_ll'] < results['su_ff'] < results['su_lf'])
        results['su_unexpected_trend'] = (results['su_ll'] < results['su_lf'] < results['su_ff'])
    else:
        results['su_expected_trend'] = None
        results['su_unexpected_trend'] = None

    if all(v is not np.nan for v in (results['du_ff'], results['du_ll'], results['du_lf'])):
        results['du_expected_trend'] = (results['du_ll'] < results['du_ff'] < results['du_lf'])
        results['du_unexpected_trend'] = (results['du_ll'] < results['du_lf'] < results['du_ff'])
    else:
        results['du_expected_trend'] = None
        results['du_unexpected_trend'] = None

    return results

if __name__ == '__main__':
    main()

