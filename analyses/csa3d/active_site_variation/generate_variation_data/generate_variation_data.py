#!/usr/bin/env python3
import pandas as pd
import numpy as np
import pickle
import sys
import random


def is_artificially_mutated(site):
    """Checks if the site has sequence agreement with its corresponding
    UniProt site. If not, it is probably mutated artificially"""
    if not site.mapped_unisites:
        return True
    for unisite in site.mapped_unisites:
        if site.sequence != unisite.sequence:
            return True
    return False

def mean_distance(site):
    """Calculates the mean inter-residue distance in a site"""
    dists = site.get_distances(kind='avg')
    if dists.size > 0:
        try:
            return round(float(np.nanmean(dists)), 2)
        except TypeError:
            return
    return

def main(infile, outfile):

    # Parse M-CSA ID
    try:
        mcsa_id = int(infile.split('/')[-1].split('.')[0].split('_')[1])
    except IndexError:
        print('Something wrong with input filename')
        return

    # Load entry
    with open(infile, 'rb') as f:
        entry = pickle.load(f)

    # Initialize dict to store data
    data = {'rms': [],
            'rms_all': [],
            'wrms_all': [],
            'rms_max': [],
            'per_res_rms': [],
            'p_mean_dist': [],
            'q_mean_dist': [],
            'mcsa_id': [],
            'p_id': [],
            'q_id': [],
            'p_is_reference': [],
            'q_is_reference': [],
            'p_sequence': [],
            'q_sequence': [],
            'p_residues': [],
            'q_residues': [],
            'p_uniprot_id': [],
            'q_uniprot_id': [],
            'p_ec': [],
            'q_ec': [],
            'p_is_conserved': [],
            'q_is_conserved': [],
            'p_is_conservative_mutation': [],
            'q_is_conservative_mutation': [],
            'p_is_artificially_mutated': [],
            'q_is_artificially_mutated': [],
            'p_hets': [],
            'q_hets': []}

    # Get RMSD values and info (ec, conservation, ligands) for each pair of active sites
    seen = set()

    sites = [site for site in entry.pdbsites if site.is_sane]
    # Limit to 500 sane sites per entry
    random.seed(13)
    if len(sites) > 500:
        sites = random.sample(sites, 500)

    for p in sites:

        # Skip artefacts
        if not p.is_sane:
            continue

        # Get ligands
        p_hets = ','.join(['{};{};{};{};{};{}'.format(i.resname, i.resid, str(i.similarity), str(i.centrality), i.type, i.is_distal) for
                           i in p.ligands])

        # Get mean inter-residue distance
        p_mean_dist = mean_distance(p)

        for q in sites:

            if not q.is_sane:
                continue

            if p == q or (q.id, p.id) in seen:
                continue
            seen.add((p.id, q.id))

            # Get ligands
            q_hets = ','.join(['{};{};{};{};{};{}'.format(i.resname, i.resid, str(i.similarity), str(i.centrality), i.type, i.is_distal) for
                               i in q.ligands])

            # Get mean inter-residue distance
            q_mean_dist = mean_distance(q)

            # Fit
            try:
                urot, utran, rms, rms_all = p.fit(q, weighted=False, cycles=10, cutoff=5, transform=False)
            except Exception as e:
                print(e)
                continue
            try:
                rot, tran, wrms, wrms_all = p.fit(q, weighted=True, scaling_factor=None, transform=False)
            except Exception as e:
                print(p.id, q.id, e)
                rot, tran, wrms, wrms_all = urot, utran, np.nan, np.nan

            if not rms:
                continue

            # Max residue pair RMSD
            try:
                rmsds = p.per_residue_rms(q, rot, tran, transform=True)
                rms_max = np.nanmax(rmsds)
                per_res_rms = ';'.join([str(r) for r in rmsds])
            except Exception as e:
                print(e)
                continue

            # Residues
            p_residues = ';'.join([''.join((r.resname, str(r.auth_resid))) for r in p])
            q_residues = ';'.join([''.join((r.resname, str(r.auth_resid))) for r in q])

            data['rms'].append(rms)
            data['rms_all'].append(rms_all)
            data['wrms_all'].append(wrms_all)
            data['rms_max'].append(rms_max)
            data['per_res_rms'].append(per_res_rms)
            data['p_mean_dist'].append(p_mean_dist)
            data['q_mean_dist'].append(q_mean_dist)
            data['mcsa_id'].append(mcsa_id)
            data['p_id'].append(p.id)
            data['q_id'].append(q.id)
            data['p_is_reference'].append(p.is_reference)
            data['q_is_reference'].append(q.is_reference)
            data['p_sequence'].append(p.sequence)
            data['q_sequence'].append(q.sequence)
            data['p_residues'].append(p_residues)
            data['q_residues'].append(q_residues)
            data['p_uniprot_id'].append(p.uniprot_id)
            data['q_uniprot_id'].append(q.uniprot_id)
            data['p_ec'].append(p.ec)
            data['q_ec'].append(q.ec)
            data['p_is_conserved'].append(p.is_conserved)
            data['q_is_conserved'].append(q.is_conserved)
            data['p_is_conservative_mutation'].append(p.is_conservative_mutation)
            data['q_is_conservative_mutation'].append(q.is_conservative_mutation)
            data['p_is_artificially_mutated'].append(is_artificially_mutated(p))
            data['q_is_artificially_mutated'].append(is_artificially_mutated(q))
            data['p_hets'].append(p_hets)
            data['q_hets'].append(q_hets)

    # Convert to Pandas dataframe for easier data handling
    data = pd.DataFrame.from_dict(data)

    # Write csv
    with open(outfile, 'w') as o:
        data.to_csv(o, sep=',', index=False, line_terminator='\n')

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
