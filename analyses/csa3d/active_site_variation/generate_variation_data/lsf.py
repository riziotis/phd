#!/usr/bin/env python3

import subprocess
import os
from datetime import datetime
from natsort import natsorted

def main():
    """Submits multiple jobs to LSF"""

    now = datetime.now().strftime('%d-%m-%y')
    indir = '/nfs/research1/thornton/riziotis/research/phd/results/csa3d/entries/res_2.0/entries'
    outdir = f'/nfs/research1/thornton/riziotis/research/phd/results/csa3d/variation/{now}/data/per_entry'
    logdir = f'/nfs/research1/thornton/riziotis/research/phd/results/csa3d/variation/{now}/logs'

    # Big memory entries
    with open('/nfs/research1/thornton/riziotis/research/csa3d/tests/bigmem.list', 'r') as f:
        bigmem = set([int(entry.strip()) for entry in f.readlines()])
    
    for dir in (outdir, logdir):
        os.makedirs(dir, exist_ok=True)

    for infile in natsorted(os.listdir(indir)):
        
        mcsa_id  = int(infile.split('.')[0].split('_')[1])

        #if mcsa_id not in (35, 246, 362, 756, 757, 798):
        #    continue

        memory = 30000
        if mcsa_id in bigmem:
            memory = 80000

        outfile = f"{infile.split('.')[0]}.csv"
        logfile = f"{infile.split('.')[0]}.log"
        
        infile = os.path.join(indir, infile)
        outfile = os.path.join(outdir, outfile)
        logfile = os.path.join(logdir, logfile)

        cmd = f"bsub -M {memory} -o {logfile} ./generate_variation_data.py {infile} {outfile}"

        subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()

