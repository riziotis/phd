#!/usr/bin/env python3
"""Script to create a dataset of sequence identity vs overall and
active site similarity over Ca atoms"""

import sys
import os
import csv
import pickle 
import numpy as np
import pandas as pd
import subprocess
from collections import defaultdict
from Bio.PDB.PDBIO import PDBIO, Select

WORKING_DIR = '/nfs/research1/thornton/riziotis/research/phd/'
if not os.path.exists(WORKING_DIR):
    WORKING_DIR = '/Users/riziotis/ebi/phd/'
ASSEMBLIES_DIR = WORKING_DIR + 'datasets/pdbe/assembly_cif/'
SEQUENCE_IDENTITIES_PICKLE = WORKING_DIR + 'datasets/mcsa/similarity_scores/sequence_alignment_nr.pickle'

# Pairwise ligand PARITY scores dataset
print('Loading sequence identities dataset')
with open(SEQUENCE_IDENTITIES_PICKLE, 'rb') as f:
    SEQUENCE_IDENTITIES = pickle.load(f)

def main(entry_path, outdir, temp_path):

    # Load entry
    print('Loading entry')
    with open(entry_path, 'rb') as entry:
        entry = pickle.load(entry)
    if len(entry.pdbsites) <= 1:
        print(f'Entry {entry.mcsa_id}: No homologues') 
        return

    # Initialize dict to store data
    data = {'mcsa_id': [],
            'is_crosschain': [],
            'reference_site': [],
            'homologous_site': [],
            'reference_resolution': [],
            'homologue_resolution': [],
            'overall_seq_identity': [],
            'overall_ca_rmsd': [],
            'catsite_ca_rmsd': [],
            'catsite_func_rmsd': []}

    # Calculate results
    ref = entry.pdbsites[0].reference_site
    is_crosschain = bool(len(set([r.chain for r in ref]))>1)
    print('Calculating scores')
    for site in entry.pdbsites:
        overall_ca_rmsd = overall_fit(ref, site, temp_path)
        overall_seq_identity = round(overall_seqsim(ref, site), 3)
        catsite_ca_rmsd = np.nan
        catsite_func_rmsd = np.nan
        try:
            _, _, _, catsite_ca_rmsd = ref.fit(site, weighted=True, ca=True)
            _, _, _, catsite_func_rmsd = ref.fit(site, weighted=True, ca=False)
        except Exception as e:
            pass

        # Put in dict
        data['mcsa_id'].append(entry.mcsa_id)
        data['is_crosschain'].append(is_crosschain)
        data['reference_site'].append(ref.id)
        data['homologous_site'].append(site.id)
        data['reference_resolution'].append(ref.resolution)
        data['homologue_resolution'].append(site.resolution)
        data['overall_seq_identity'].append(overall_seq_identity)
        data['overall_ca_rmsd'].append(overall_ca_rmsd)
        data['catsite_ca_rmsd'].append(catsite_ca_rmsd)
        data['catsite_func_rmsd'].append(catsite_func_rmsd)

    # Convert to pandas DataFrame
    print('Converting to dataframe')
    data = pd.DataFrame.from_dict(data)

    # Write csv
    print('Writing csv')
    with open(os.path.join(outdir, 'csa3d_{}.sim_vs_rms.csv'.format(str(entry.mcsa_id).zfill(4))), 'w') as o:
        data.to_csv(o, sep=',', index=False, line_terminator='\n')
    print('Finished')

def overall_seqsim(site_p, site_q):
    """Gets the average sequence identity of the sequences that make
    up the active site"""
    seen = set()
    sims = []
    if site_p == site_q:
        return 1
    for res_p, res_q in zip(site_p, site_q):
        if res_p.is_gap or res_q.is_gap:
            continue
        ch_p = '{}_{}'.format(res_p.pdb_id, res_p.chain[0])
        ch_q = '{}_{}'.format(res_q.pdb_id, res_q.chain[0])
        if (ch_p, ch_q) in seen or (ch_q, ch_p) in seen:
            continue
        seen.add((ch_p, ch_q))
        identity = SEQUENCE_IDENTITIES.get((ch_p, ch_q), 
                        SEQUENCE_IDENTITIES.get((ch_q, ch_p), np.nan))
        sims.append(identity)
    return np.nanmean(np.array(sims, dtype='float32'))

def overall_fit(site_p, site_q, temp_path='.'):
    """Superimposes the corresponding homologous chains from 
    the sites to find the overall rmsd"""
    io = PDBIO(QUIET=True)
    seen = set()
    rmsds = []
    if site_p == site_q:
        return 0
    for res_p, res_q in zip(site_p, site_q):
        if res_p.is_gap or res_q.is_gap:
            continue
        ch_p, ch_q = res_p.chain, res_q.chain
        if (ch_p, ch_q) in seen or (ch_q, ch_p) in seen:
            continue
        seen.add((ch_p, ch_q))
        try:
            #Write temporary PDBs for each chain
            path_p = os.path.join(temp_path, '{}_{}.p.pdb'.format(site_p.mcsa_id, site_p.id))
            path_q = os.path.join(temp_path, '{}_{}.q.pdb'.format(site_q.mcsa_id, site_q.id))
            io.set_structure(site_p.parent_structure)
            io.save(path_p, ChainSelect([ch_p]))
            io.set_structure(site_q.parent_structure)
            io.save(path_q, ChainSelect([ch_q]))
            # TM-Align the two chains
            rms = tm_rms(path_p, path_q)
            os.remove(path_p)
            os.remove(path_q)
        except Exception:
            rms = np.nan
        rmsds.append(rms)
    return np.nanmean(np.array(rmsds))

class ChainSelect(Select): 
    """to select pdb output"""
    def __init__(self, chains):
        super(ChainSelect, self).__init__()
        self.chains = tuple(chains)
    def accept_residue(self, residue):
        if residue.parent.id in self.chains:
            return 1
        else:
            return 0  

def tm_rms(pdb1, pdb2):
    """Superimposes two pdb structures using TM-align and returns RMSD"""
    p = subprocess.run('tmalign {} {} 2>/dev/null | grep RMSD= '.format(pdb1, pdb2),
            stdout=subprocess.PIPE,
            shell=True)
    tmalignOut = p.stdout.decode('utf-8').strip()
    try:
        rms = float(tmalignOut.split()[4].strip(','))
        return rms
    except Exception:
        return np.nan

if __name__ == '__main__':
   main(sys.argv[1], sys.argv[2], sys.argv[3]) 
