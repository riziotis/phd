#!/usr/bin/env python3
"""Reads cif file from stdin and outputs ATOM and HETATM lines in pdb format
Automatically detects if the structure is NRM-derived and writes MODEL and ENDMDL
atomIDs. Standalone program. If you want to incorporate it as function use the one
from the various_classes_functions module"""

import sys
import re
sys.path.insert(0, '/Users/riziotis/ebi/phd/src/')
from templates.common import transform_chain

is_nmr = False
previous_nmrModel = 0

for line in sys.stdin: 
    
    #check if nmr ensemble
    if re.search('nmr', line): 
        is_nmr = True
        continue

    if re.search('^ATOM|HETATM', line):
    
        input_line = line.split()

        prefix = input_line[0]
        atomID = int(input_line[1])
        atomName =  re.sub("\'|\"", "", input_line[3])
        altAtom = input_line[4] #if atom occupancy is <1.0, this is the alternative atom identifier
        resName = input_line[5]
        resChain = transform_chain(input_line[18], to_dash = False)
        authResid = input_line[21]
        coordX = float(input_line[10])
        coordY = float(input_line[11])
        coordZ = float(input_line[12])
        occupancy = float(input_line[13])
        b_factor = float(input_line[14])
        nmrModel = int(input_line[20])

        line = ('{:6}{:5d} {:<4}{}{:>3}{:>2}{:>4}{:>12.3f}'
                '{:>8.3f}{:>8.3f}{:>6.2f}{:>6.2f}{:>5}'.format(
               prefix,
               atomID,
               atomName if len(atomName) == 4 else ' {}'.format(atomName),
               altAtom if altAtom != '.' else ' ',
               resName.upper(),
               resChain,
               authResid,
               coordX,
               coordY,
               coordZ,
               occupancy,
               b_factor,
               nmrModel))

        if (is_nmr and previous_nmrModel == 0):
            print ("MODEL {}".format(nmrModel))        

        if (is_nmr and nmrModel > previous_nmrModel and previous_nmrModel != 0):
            print ("ENDMDL" "\n" "MODEL {}".format(nmrModel))
      
       #  Uncomment if TER is needed for last residue in NMR ensemble
       #
       # if (nmrModel > previous_nmrModel and previous_nmrModel != 0):
       #     print ("TER{:>8d}  {:<7}{:>2}{:>4}" "\n" "ENDMDL" "\n" "MODEL {}".format(
       #         atomID+1,
       #         resName.upper(),
       #         resChain,
       #         authResid,
       #         nmrModel))
        print (line)

        previous_nmrModel = nmrModel

if(is_nmr):
    print("ENDMDL")
