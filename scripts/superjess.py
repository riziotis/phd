#!/usr/bin/env python3

import sys
import os
import re
import subprocess
from glob import glob


def main():
    try:
        str1 = sys.argv[1]
        str2 = sys.argv[2]

        #Write list files
        with open("fixed.temp", "w") as f:
            print(str2, file=f)
        with open("moving.temp", "w") as f:
            print(str1, file=f)
    except:
        print('Check your PDBs. Bye.')
        quit()

    #Run Jess
    try:
        with open('jessOut.temp', 'w+') as jessOut:
            subprocess.run('jess moving.temp fixed.temp 10 10',
                    stdout=jessOut,
                    shell=True)
            jessOut.seek(0)        
            rmsd = float(filter_jess_out(jessOut)[0].split()[2])
            print (rmsd)
    except:
        print('Could not superimpose. Bye.')
        pass

    for tempfile in glob('*.temp'):
        if os.path.isfile(tempfile):
            os.remove(tempfile)

class Atom():
    """Atom class containing, entry ID, atom ID, name, residue, chain, resid and coordinates x,y,z"""

    def __init__(self, pdbID, entryID, atomID, name, residue, chain, resID, x, y, z):
        self.pdbID = pdbID
        self.entryID = entryID
        self.atomID = atomID
        self.name = name.strip()
        self.residue = residue
        self.chain = chain 
        self.resID = resID
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        return __str__(self)

    def __str__(self):
        return "{0.entryID} {0.atomID} {0.residue} {0.name} {0.pdbID}".format(self)
        
    def distance(self, atom_i):
        """Calculates euclidean distance between current atom and atom in argument, using coordinates x,y,z"""
        return math.sqrt((self.x - atom_i.x)**2 + (self.y - atom_i.y)**2 + (self.z - atom_i.z)**2)

def pdb_to_atom(pdbLine, pdbID="N/A"):
    """Reads an ATOM or HETATM line from a pdb file and outputs an Atom() object"""
    
    if re.search('^ATOM|HETATM', pdbLine):
        atom = Atom(
                pdbID,          #pdbID
                pdbLine[0:6],   #entryID
                pdbLine[6:11],  #atomID
                pdbLine[13:16], #name
                pdbLine[17:20], #residue
                pdbLine[20:22], #chain
                pdbLine[22:27], #resid
                float(pdbLine[30:38]), #coord x
                float(pdbLine[38:46]), #coord y
                float(pdbLine[46:54])) #coord z
        return atom
    return False

def filter_jess_out(jess_out):
    """Takes the output of Jess and if there are multiple optimal alignments in specific position
    in a given template-query pair, it keeps only the best one (lowest LogE)"""

    logE_values = []
    templates = dict()
    chains = []
    current_template = []
    previous_target = None
    previous_chains = set() 
    filtered_out = []

    for line in jess_out: 
        if (line.startswith('REMARK')):
            current_template.append(line)
            remark_fields = line.split()
            target = remark_fields[1]
            query = remark_fields[3]
            logE = float(remark_fields[-1])
            logE_values.append(logE)

        if (line.startswith('ATOM')):
            atom = pdb_to_atom(line)
            #Add alternative residues
            current_template.append(line)
            chains.append(atom.chain.strip())

        if (line.startswith('ENDMDL')):
            chains = tuple(chains)
            current_template.append(line)

            if previous_target == target and previous_chains == chains:
                if logE == min(logE_values):
                    templates[target, chains] = current_template.copy()
            if previous_target != target or (previous_target == target and previous_chains != chains):
                templates[target, chains] = current_template.copy()
                logE_values.clear()

            current_template.clear()
            previous_target = target
            previous_chains = chains
            chains = []

    for key, template in templates.items():
        for line in template:   
            filtered_out.append(line.strip())
        filtered_out.append('')

    return filtered_out

main()
