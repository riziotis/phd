#!/usr/bin/env python3
"""Simple script to calculate RMSD between to structures with
the ***same*** number of atoms. It does not align, it does
not superimpose, it only computes RMSD for validation."""

import sys
import math
sys.path.insert(0, '/Users/riziotis/ebi/phd/src/')
from templates.common import pdb_get_residues

try:
    pdb_a = pdb_get_residues(sys.argv[1])
    pdb_b = pdb_get_residues(sys.argv[2])
except:
    print('Check your PDBs, bye.', file=sys.stderr)
    quit()

nofres = len(pdb_a)
nofatoms = 0
total = 0
for i in range(nofres):
    for j in range(len(pdb_a[i])):
        nofatoms += 1
        a_name = pdb_a[i][j].name
        b_name = pdb_b[i][j].name
        a_x = float(pdb_a[i][j].x) 
        a_y = float(pdb_a[i][j].y) 
        a_z = float(pdb_a[i][j].z) 
        b_x = float(pdb_b[i][j].x)
        b_y = float(pdb_b[i][j].y)
        b_z = float(pdb_b[i][j].z)

        #print('{} {}   {:5.2} {:5.2} {:5.2}'.format(a_name, b_name, abs(a_x - b_x), abs(a_y - b_y), abs(a_z - b_z)))

        total += (a_x - b_x)**2 + (a_y - b_y)**2 + (a_z - b_z)**2

rmsd = math.sqrt(total/nofatoms)
print(round(rmsd, 3))
       

