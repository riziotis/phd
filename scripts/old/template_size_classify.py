#!/usr/bin/env python

#python imports
import math
import sys
import os
#project imports
from various_classes_functions import Atom, Residue, pdb_get_residues, pdb_to_atom


def main():

    dir_path = sys.argv[1]
    size = int(sys.argv[2])
    if len(sys.argv) > 3:
        greater = True
    else:
        greater  = False

    for file_ in os.listdir(dir_path):
        pdb = pdb_get_residues('{}{}'.format(dir_path,file_))
        if greater:
            if len(pdb) < size:
                print (file_)
        else:
            if len(pdb) == size:
                print(file_)
main()
