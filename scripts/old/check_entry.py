#!/usr/bin/env python

#python imports
import sys
import math
import re
import os.path
#local imports
from various_classes_functions import Atom, get_cif_file, cif_to_atom

def main():

    ca_atoms = [[] for _ in range(10)]
    alt_cif = [] #list to store each cif file
    hitsDefault = []
    hitsAlternative = []
    previous_pdbID = None
    nofAssemblies = 0

    for line in sys.stdin: #read distances file of type '[resid1 res1 ch1 resid2 res2 ch2 ca_distance pdb_filename]'

        distDefault = None
        distAlternative = None

        inLine = line.split()
        in_atom1_resID = inLine[0]
        in_atom1_residue = inLine[1]
        in_atom1_chain = inLine[2]
        in_atom2_resID = inLine[3]
        in_atom2_residue = inLine[4]
        in_atom2_chain = inLine[5]
        in_distance = float(inLine[6])
        in_pdbID = re.search(r'([0-9]+)_(([0-9]|[a-z])+)_', inLine[7]).group(2) #get only pdbID from filename
        
        if (in_distance) < 10: #Distance cutoff to process entry
            continue

        #First step: In default assembly (saved locally), check alternative chains 
        if(previous_pdbID != in_pdbID or previous_pdbID == None):
            if (previous_pdbID != None):
                if (len(hitsDefault) > 0 or len(hitsAlternative) > 0):
                    print(previous_pdbID, ":\n   In default assembly:")
                    for _ in hitsDefault: #print report for whole previous entry
                        print(_)
                    print('   Found {} alternative assemblies:'.format(nofAssemblies-1))
                    for _ in hitsAlternative:
                        print(_)
                    hitsDefault.clear()
                    hitsAlternative.clear()
                    nofAssemblies = 0

            default_cif_filename = '/home/riziotis/mcsa_project/dataset/biol_assembly_default_cif/{}_assembly.cif'.format(in_pdbID)
            if not os.path.isfile(default_cif_filename): 
                continue #WILL REMOVE THIS
            with open(default_cif_filename, 'r') as cif: #write all ca atoms from default biol assembly in ca_atoms[0]
                ca_atoms[0].clear()
                for cif_line in cif:
                    if re.search('^ATOM|HETATM', cif_line):
                        cif_fields = cif_line.split()
                        if (cif_fields[3] == 'CA'):
                            atom = cif_to_atom(cif_line, in_pdbID)
                            ca_atoms[0].append(atom) 
                
        for atom1 in ca_atoms[0]:
            for atom2 in ca_atoms[0]:
                if(atom1.resID == in_atom1_resID
                        and atom1.residue == in_atom1_residue
                        and atom2.resID == in_atom2_resID
                        and atom2.residue == in_atom2_residue
                        and atom1.chain == in_atom1_chain):
                    d = round(atom1.distance(atom2), 1)
                    if(d < round(in_distance, 1)-0.10 and d > 0):
                        distDefault = d
                        hitsDefault.append('        d({0.resID:>4} {0.residue} {0.chain:>3}, '\
                                           '{1.resID:>4} {1.residue} {1.chain:>3})={2:>5.2f} < '\
                                           'd({3:>4} {4} {5:>3}, {6:>4} {7} {8:>3})={9:>5.2f}'.format(
                                                atom1, 
                                                atom2, 
                                                round(d,1), 
                                                in_atom1_resID, 
                                                in_atom1_residue, 
                                                in_atom1_chain,
                                                in_atom2_resID,
                                                in_atom2_residue,
                                                in_atom2_chain,
                                                round(in_distance, 1))) 

        #Second step: Check alternative assemblies
        if(previous_pdbID != in_pdbID or previous_pdbID == None):
            for i in range(1,10): #get ca atoms from alternative assemblies if present
                assembly_not_found = False
                ca_atoms[i].clear()
                alt_cif = get_cif_file(in_pdbID, i).splitlines()
                for cif_line in alt_cif:
                    if re.search('not found', cif_line): #see if assembly exists
                        nofAssemblies = i-1
                        assembly_not_found = True
                        break
                    if re.search('^ATOM|HETATM', cif_line):
                        cif_fields = cif_line.split()
                        if (cif_fields[3] == 'CA'):
                            atom = cif_to_atom(cif_line, in_pdbID)
                            ca_atoms[i].append(atom) 
                if assembly_not_found == True:
                    break
            
        for i in range(1,10):
        #Check if assembly is the same with default one
            is_default = True
            if len(ca_atoms[0]) != len(ca_atoms[i]):
                is_default = False
            else:
                for j in range(len(ca_atoms[0])):
                    if str(ca_atoms[i][j]) != str(ca_atoms[0][j]):
                        is_default = False
            if is_default == True:
                continue

            for atom1 in ca_atoms[i]:
                for atom2 in ca_atoms[i]:
                    if(atom1.resID == in_atom1_resID
                            and atom1.residue == in_atom1_residue
                            and atom1.chain == in_atom1_chain
                            and atom2.resID == in_atom2_resID
                            and atom2.residue == in_atom2_residue):
                        d = round(atom1.distance(atom2), 1)
                        if(d < round(in_distance, 1)-0.10 and d > 0):
                            distAlternative = d
                            hitsAlternative.append('        In assembly {0}, d({1.resID:>4} {1.residue} {1.chain:>3}, '\
                                                   '{2.resID:>4} {2.residue} {2.chain:>3})={3:>5.2f} < '\
                                                   'd({4:>4} {5} {6:>3}, {7:>4} {8} {9:>3})={10:>5.2f}'.format(
                                                        i,
                                                        atom1, 
                                                        atom2, 
                                                        round(d,1), 
                                                        in_atom1_resID, 
                                                        in_atom1_residue, 
                                                        in_atom1_chain,
                                                        in_atom2_resID,
                                                        in_atom2_residue,
                                                        in_atom2_chain,
                                                        round(in_distance, 1)))
            

        previous_pdbID = in_pdbID

if __name__ == "__main__":
    main()
