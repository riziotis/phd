#!/usr/bin/env python

#python imports
import math
import sys
import os
#project imports
from various_classes_functions import Atom, Residue, pdb_get_residues, pdb_to_atom
           
def run():
    """Reads every pdb file containing catalytic residues and calculates distances between CAs"""
    
    #Parse command line arguments 
    try:
        len(sys.argv) in [2,3]
        isinstance(sys.argv[1], str)
        if len(sys.argv) == 3:
            sys.argv[2] == '--average'
            
    except:
        print ("Usage: calc_atom_distances.py [template directory] [options]"
               "        Option: --average: calculate average Ca distance for"
               "                           each input template")
    dir_path = sys.argv[1]
    if len(sys.argv) == 3:
        average = True
    else:
        average = False
    
    #Main program
    distances = [] #Stores distance and residue info 
    buffer_ = [] #Buffer to exclude duplicate distance values (atom1-atom2 = atom2-atom1)
    for file_ in os.listdir(dir_path):
        filename = '{}{}'.format(dir_path, file_)
        distances.clear()
        buffer_.clear()
        distSum = 0

        residues = pdb_get_residues(filename, file_)
            
        for residue1 in residues:
            for residue2 in residues:
                d = residue1.distance(residue2)
                if residue1 != residue2 and d not in buffer_:
                    distances.append('{0.resID}\t{0.name}\t{0.chain}\t{1.resID}\t{1.name}\t{1.chain}\t{2:>5.2f}\t{3.pdbID}'.format(residue1, residue2, d, residue1))
                    buffer_.append(d)
                    distSum += d

        if average and len(residues) > 1:
            distAverage = distSum/len(distances)
            print('{0.pdbID} average catalytic Ca distance: {1:<6.3f}'.format(atom1, distAverage))
        else:
            for i in distances:
                print(i)

if __name__ == "__main__":
    run()
    
